#use wml::debian::blend title="Sobre o Blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="794fc406da417697ec055ef6c78e0cff7510f391"

<p>O <b>Debian GIS Pure Blend</b> é um projeto do
<a href="https://wiki.debian.org/Teams/DebianGis">Time GIS do Debian</a> que
colabora mantendo pacotes relacionados ao GIS (Sistema de Informação
Geográfica) para o Debian.
Todo <a href="https://blends.debian.org/">Pure Blend</a> é um subconjunto
do Debian que é configurado para ajudar imediatamente um público-alvo específico.
Este <i>blend</i> visa apoiar as necessidades de pessoas que trabalham com mapas,
sensoriamento remoto e observação da Terra.</p>

<p>O <i>Blend</i> é construído a partir de uma lista de programas GIS
selecionados para o Debian.
Os dois principais resultados deste <i>Blend</i> são uma coleção de "metapacotes"
e imagens de instalação "live" que podem ser gravadas em DVD ou pendrive.</p>

<h2>Metapacotes</h2>

<p>Os metapacotes no Debian são pacotes que pode ser instalados exatamente como outros
pacotes mas que não contêm softwares neles, instruindo o sistema de empacotamento
a instalar um grupo de outros pacotes.</p>

<p>Veja <a href="./get/metapackages">usando os metapacotes</a> para mais
informações sobre quais metapacotes estão disponíveis e sobre como instalar um
metapacote em um sistema Debian.</p>
