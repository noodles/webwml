#use wml::debian::cdimage title="Instalacja sieciowa z małej płyty CD"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="cf13aff27aee3763a70db6bb4c8d2087bea5716e"
#include "$(ENGLISHDIR)/releases/images.data"

<p><q>Instalacja sieciowa</q> lub po angielsku w skrócie <q>netinst</q> jest
pojedynczą płytą CD, która pozwala zainstalować cały system operacyjny. Płyta
ta zawiera tylko minimalny zestaw oprogramowania, aby móc rozpocząć instalację
i pobrać pozostałe pakiety z Internetu.</p>

<p><strong>Co jest lepsze &mdash; mała płyta sieciowa, czy pełen zestaw
płyt?</strong> To zależy, ale sądzimy, że w wielu przypadkach obraz małej
płyty jest lepszy &mdash; poza wszystkim, ściągasz tylko te pakiety,
które wybrałeś do instalacji na Twojej maszynie, co oszczędza zarówno czas,
jak i pasmo sieciowe. Z drugiej strony, pełne zestawy są bardziej odpowiednie,
jeśli instalujesz system na więcej niż jednej maszynie lub bez dostępu do
Internetu.</p>

<p><strong>Jakie typy dostępu do sieci są możliwe w czasie
instalacji?</strong>
Instalacja sieciowa zakłada, że masz połączenie z Internetem. Obsługiwane
są różne rodzaje połączeń, na przykład modem analogowy PPP,
Ethernet, WLAN (z pewnymi ograniczeniami), ale nie ISDN &mdash; przykro nam!</p>

<p>Są dostępne do pobrania następujące bootowalne obrazy płyt
minimalnych:</p>

<ul>

  <li>Oficjalne obrazy <q>netinst</q> dla edycji <q>stabilnej</q> &mdash; <a
  href="#netinst-stable">patrz poniżej</a></li>

  <li>Obrazy edycji <q>testing</q>, zarówno dzienne, jak i znane działające
  kompilacje, zobacz stronę <a
  href="$(DEVEL)/debian-installer/">Debian-Installer</a>.</li>

</ul>


<h2 id="netinst-stable">Oficjalne obrazy typu <q>netinst</q> wydania <q>stabilnego</q></h2>

<p>Liczący do 300&nbsp;MB obraz zawiera instalator i niewielki
zestaw pakietów, które pozwalają na zainstalowanie (bardzo) podstawowego
systemu.</p>

<div class="line">
<div class="item col50">
<p><strong>obraz płyty CD <q>netinst</q> (150-300 MB, w zależności od architektury)</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
obraz płyty CD <q>netinst</q> (przez <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>


<p>Więcej informacji na temat obrazów płyt i ich użycia znajduje się w
<a href="../faq/">często zadawanych pytaniach - FAQ</a>.</p>

<p>Po pobraniu obrazów polecamy
<a href="$(HOME)/releases/stable/installmanual">szczegółowe
informacje na temat procesu instalacji</a>.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Nieoficjalne obrazy netinst z dołączonym niewolnym oprogramowaniem firmware</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w <a href="../../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy instalacyjne dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>
