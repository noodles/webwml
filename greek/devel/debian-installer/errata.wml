#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="50ddc8fab8f8142c1e8266a7c0c741f9bfe1b23a" maintainer="galaxico"

<h1>Errata for <humanversion /></h1>

<p>
This is a list of known problems in the <humanversion /> release of the Debian
Installer. If you do not see your problem listed here, please send us an
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installation
report</a> describing the problem.
</p>

<dl class="gloss">
     <dt>Broken rescue mode with the graphical installer</dt>
     <dd>It was discovered during Bullseye RC 1 image testing that
     the rescue mode seems broken (<a href="https://bugs.debian.org/987377">#987377</a>).
     Additionally, the “Rescue” label in the banner needs to be
     adjusted for the Bullseye theme.
     <br />
     <b>Status:</b> Fixed in Bullseye RC 2.</dd>

     <dt>amdgpu firmware required for many AMD graphic cards</dt>
     <dd>There seems to be a growing need to install amdgpu firmware
     (through the non-free <code>firmware-amd-graphics</code> package)
     to avoid a black screen when booting the installed system. As of
     Bullseye RC 1, even when using an installation image that includes
     all firmware packages, the installer doesn't detect the need for
     that specific component. See the
     <a href="https://bugs.debian.org/989863">umbrella bug report</a>
     to keep track of our efforts.
     <br />
     <b>Status:</b> Fixed in Bullseye RC 3.</dd>

     <dt>Firmwares required for some sound cards</dt>
     <dd>There seems to be a number of sound cards that require loading a
     firmware to be able to emit sound. As of Bullseye, the installer is
     not able to load them early, which means that speech synthesis during
     installation is not possible with such cards. A possible workaround is to
     plug another sound card which does not need such firmware.
     See the <a href="https://bugs.debian.org/992699">umbrella bug report</a>
     to keep track of our efforts.</dd>

     <dt>Desktop installations may not work using CD#1 alone</dt>
     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead. <br /> <b>Status:</b> It is
     unlikely more efforts can be made to fit more packages on
     CD#1. </dd>

     <dt>LUKS2 is incompatible with GRUB's cryptodisk support</dt>
     <dd>It was only lately discovered that GRUB has no support for
       LUKS2. This means that users who want to use
       <tt>GRUB_ENABLE_CRYPTODISK</tt> and avoid a separate,
       unencrypted <tt>/boot</tt>, won't be able to do so
       (<a href="https://bugs.debian.org/927165">#927165</a>).  This
       setup isn't supported in the installer anyway, but it would
       make sense to at least document this limitation more
       prominently, and to have at least the possibility to opt for
       LUKS1 during the installation process.
     <br />
     <b>Status:</b> Some ideas have been expressed on the bug; cryptsetup maintainers have written <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">some specific documentation</a>.</dd>

</dl>
