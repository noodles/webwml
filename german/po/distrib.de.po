# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Gerfried Fuchs <alfie@ist.org>, 2004.
# Helge Kreutzmann <debian@helgefjell.de>, 2007.
# Dr. Tobias Quathamer <toddy@debian.org>, 2005, 2007, 2011, 2012, 2016, 2017, 2019.
# Holger Wansing <linux@wansing-online.de>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml distrib\n"
"PO-Revision-Date: 2019-02-14 14:26+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Schlüsselwort"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Zeige an"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "Pfade, die mit Suchwort enden"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "Pakete mit Dateien, die so benannt sind"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "Pakete mit Dateien, deren Namen das Suchwort enthalten"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distributionen"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "Experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "Unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "Testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "Stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "Oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Architektur"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "alle"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Suchen"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Zurücksetzen"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Suche nach"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Nur Paketnamen"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Beschreibungen"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Quellpaket-Namen"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Zeige nur exakte Treffer"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Abschnitte"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-Bit-PC (AMD64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64-Bit-ARM (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd 32-Bit-PC (i386)"

#: ../../english/releases/arches.data:16
#, fuzzy
#| msgid "64-bit PC (amd64)"
msgid "Hurd 64-bit PC (amd64)"
msgstr "64-Bit-PC (AMD64)"

#: ../../english/releases/arches.data:17
msgid "32-bit PC (i386)"
msgstr "32-Bit-PC (i386)"

#: ../../english/releases/arches.data:18
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32-Bit-PC (i386)"

#: ../../english/releases/arches.data:20
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64-Bit-PC (AMD64)"

#: ../../english/releases/arches.data:21
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:22
msgid "MIPS (big endian)"
msgstr "MIPS (Big endian)"

#: ../../english/releases/arches.data:23
msgid "64-bit MIPS (little endian)"
msgstr "64-Bit MIPS (Little endian)"

#: ../../english/releases/arches.data:24
msgid "MIPS (little endian)"
msgstr "MIPS (Little endian)"

#: ../../english/releases/arches.data:25
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:26
msgid "POWER Processors"
msgstr "POWER Prozessoren"

#: ../../english/releases/arches.data:27
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V 64-Bit Little endian (riscv64)"

#: ../../english/releases/arches.data:28
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:29
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:30
msgid "SPARC"
msgstr "SPARC"
