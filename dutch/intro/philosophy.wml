#use wml::debian::template title="Onze filosofie: waarom we het doen en hoe we het doen"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Onze Missie: Een vrij besturingssysteem maken</a></li>
    <li><a href="#how">Onze waarden: hoe de Debian-gemeenschap werkt</a></li>
  </ul>
</div>

<h2><a id="freedom">Onze Missie: Een vrij besturingssysteem maken</a></h2>

<p>Het Debian-project is een vereniging van individuen die een
gemeenschappelijk doel nastreven: wij willen een vrij besturingssysteem maken
dat vrij beschikbaar is voor iedereen. Welnu, wanneer we het woord "vrij"
gebruiken, hebben we het niet over geld, maar over software-<em>vrijheid</em>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Onze definitie van vrije software</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">De verklaring van de Free Software Foundation lezen</a></button></p>

<p>Misschien vraagt u zich af waarom zoveel mensen ervoor kiezen om vele uren
van hun eigen tijd te besteden aan het schrijven van software, deze zorgvuldig
te verpakken en te onderhouden, om dit vervolgens allemaal weg te geven zonder
er geld voor te vragen. Welnu, er zijn genoeg redenen, hier zijn er enkele:</p>

<ul>
  <li>Sommige mensen vinden het gewoon leuk om anderen te helpen, en bijdragen aan een project voor vrije software is een geweldige manier om dit te verwezenlijken.</li>
  <li>Veel ontwikkelaars schrijven programma's om meer te leren over computers, verschillende architecturen en programmeertalen.</li>
  <li>Sommige ontwikkelaars dragen bij om "dank u" te zeggen voor alle geweldige vrije software die ze van anderen hebben ontvangen.</li>
  <li>Veel mensen in de academische wereld maken vrije software om de resultaten van hun onderzoek te delen.</li>
  <li>Ook bedrijven helpen bij het onderhouden van vrije software: om invloed uit te oefenen op de manier waarop toepassingen zich ontwikkelen of om snel nieuwe functies te implementeren.</li>
  <li>De meeste ontwikkelaars van Debian doen natuurlijk mee, omdat ze vinden dat het erg leuk is om te doen!</li>
</ul>

<p>Hoewel wij geloven in vrije software, respecteren wij dat mensen soms
niet-vrije software op hun machines moeten installeren - of ze dat nu willen of
niet. We hebben besloten om deze gebruikers waar mogelijk te ondersteunen. Er
is een groeiend aantal pakketten dat niet-vrije software installeert op een
Debian systeem.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> We zijn toegewijd aan vrije software en we hebben die toewijding geformaliseerd in een document: ons <a href="$(HOME)/social_contract">Sociaal Contract</a></p>
</aside>

<h2><a id="how">Onze waarden: hoe de gemeenschap werkt</a></h2>

<p>Het Debian-project heeft meer dan duizend actieve <a
href="people">ontwikkelaars en medewerkers</a>, verspreid over <a
href="$(DEVEL)/developers.loc">de wereld</a>. Een project van deze
omvang heeft een zorgvuldig uitgewerkte
<a href="organization">organisatiestructuur</a> nodig.
Dus, als u zich afvraagt hoe het Debian-project werkt en of de
Debian-gemeenschap regels en richtlijnen heeft, bekijk dan eens de volgende
verklaringen:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">De statuten van Debian</a>: <br>
          Dit document beschrijft de organisatiestructuur en legt uit hoe het Debian-project formele beslissingen neemt.</li>
        <li><a href="../social_contract">Het sociaal contract en de richtlijnen voor vrije software</a>: <br>
          Het sociaal contract van Debian en de richtlijnen voor vrije software (Debian Free Software Guidelines - DFSG) als een onderdeel van dit contract, beschrijven onze toewijding aan vrije software en de aan vrije-softwaregemeenschap.</li>
        <li><a href="diversity">De diversiteitsverklaring:</a> <br>
          Het Debian-project verwelkomt iedereen en moedigt iedereen aan om mee te werken, ongeacht hoe u zichzelf identificeert of hoe anderen u zien.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">De gedragscode:</a> <br>
          Wij hebben een gedragscode aangenomen voor wie deelneemt aan onze mailinglijsten, IRC-kanalen, enz.</li>
        <li><a href="../doc/developers-reference/">De referentiehandleiding voor ontwikkelaars:</a> <br>
          Dit document geeft een overzicht van de aanbevolen procedures en de beschikbare bronnen voor ontwikkelaars en onderhouders van Debian.</li>
        <li><a href="../doc/debian-policy/">Het beleidshandboek van Debian:</a> <br>
          Een handleiding die de beleidsvereisten voor de Debian-distributie beschrijft, bijv. de structuur en inhoud van het Debian-archief, technische vereisten waaraan elk pakket moet voldoen om te worden opgenomen, enz.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Binnenin Debian: <a href="$(DEVEL)/">Ontwikkelaarshoek</a></button></p>

