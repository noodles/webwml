#use wml::debian::cdimage title="Netwerkinstallatie vanaf een minimale cd"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="c64bdf15d82617c46942470ac0846ed7ec8b0f74"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<p>Een <q>netwerkinstallatie</q>- of <q>netinst</q>-cd is één cd die u in staat
stelt om het volledige besturingssysteem te installeren. Deze ene cd bevat
enkel de minimale hoeveelheid aan software om het basissysteem te installeren
en de overige pakketten op te halen via het internet.</p>

<p><strong>Wat is beter voor mij &mdash; de minimale opstartbare cd of de
volledige cd’s?</strong>. Dat hangt ervan af, maar wij denken dat in de meeste
gevallen de minimale cd beter is. U hoeft alleen die pakketten die u tijdens de
installatie voor uw machine selecteert, te downloaden. Dit spaart zowel tijd als
bandbreedte. De volledige cd’s zijn geschikter als u meerdere machines wilt
installeren, of een machine zonder goedkope internetverbinding.</p>

<p><strong>Welke types van netwerkverbindingen worden ondersteund tijdens de
installatie?</strong>
De netwerkinstallatie gaat ervan uit dat u een verbinding met internet heeft.
Hierbij worden verschillende manieren ondersteund, zoals een analoge
PPP-inbelverbinding, ethernet en WLAN (met enkele beperkingen), maar geen ISDN
&mdash; sorry!</p>

<p>De volgende minimale opstartbare cd-images kunnen gedownload worden:</p>

<ul>

<li>Officiële "netinst" images voor de "stable" release &mdash;
<a href="#netinst-stable">zie hieronder</a></li>

<li>Images voor de release <q>testing</q> &mdash; zie de
<a href="$(DEVEL)/debian-installer/">Debian-Installer pagina</a>.</li>

</ul>

<h2 id="netinst-stable">Officiële netinstallatie-images voor de
<q>stable</q> release</h2>

<p>Dit image bevat het installatiesysteem en een klein
aantal pakketten dat de installatie van een (erg) basaal systeem toelaat.</p>

<div class="line">
<div class="item col50">
<p><strong>
netinst cd-image
</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
netinst cd-image (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Voor informatie over deze bestanden en hoe ze te gebruiken, zie de
<a href="../faq/">FAQ</a>.</p>

<p>Vergeet na het downloaden van de images niet om ook de
<a href="$(HOME)/releases/stable/installmanual">gedetailleerde informatie over
het installatieproces</a> te bekijken.</p>

