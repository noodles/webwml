#use wml::debian::template title="Sleutels overzetten"
#use wml::debian::translation-check translation="5011f532637dc7820b79b151eecfda4ab65aa22f"

<p>
In het <a href="$(HOME)/security/2008/dsa-1571">beveiligingsadvies 1571 van Debian</a>, heeft het veiligheidsteam van Debian een zwakte onthuld in de generator van willekeurige getallen die gebruikt wordt door OpenSSL op Debian en zijn derivaten. Als gevolg van deze zwakte komen bepaalde encryptiesleutels veel vaker voor dan het geval zou moeten zijn, zodat een aanvaller met minimale kennis van het systeem de sleutel kan raden via een aanval met brute kracht. Dit heeft vooral gevolgen voor het gebruik van encryptiesleutels in OpenSSH, OpenVPN en SSL-certificaten.
</p>

<p>
Op deze pagina wordt beschreven hoe u de procedures voor het overzetten van sleutels uitvoert voor pakketten waarvan de sleutels zijn aangetast door de OpenSSL-kwetsbaarheid.
</p>

<ul>
<li><b><a href="#openssh">OpenSSH</a></b></li>
<li><b><a href="#openssl">OpenSSL: Algemene instructies voor het genereren van PEM-sleutels</a></b></li>

<li><a href="#bincimap">bincimap</a></li>
<li><a href="#boxbackup">boxbackup</a></li>
<li><a href="#cryptsetup">cryptsetup</a></li>
<li><a href="#dropbear">dropbear</a></li>
<li><a href="#ekg">ekg</a></li>
<li><a href="#ftpd-ssl">ftpd-ssl</a></li>
<li><a href="#gforge">gforge</a></li>
<li><a href="#kerberos">MIT Kerberos (krb5)</a></li>
<li><a href="#nessus">Nessus</a></li>
<li><a href="#openswan">OpenSWAN / StrongSWAN</a></li>
<li><a href="#openvpn">OpenVPN</a></li>
<li><a href="#proftpd">Proftpd</a></li>
<li><a href="#puppet">puppet</a></li>
<li><a href="#sendmail">sendmail</a></li>
<li><a href="#ssl-cert">ssl-cert</a></li>
<li><a href="#telnetd-ssl">telnetd-ssl</a></li>
<li><a href="#tinc">tinc</a></li>
<li><a href="#tor">tor</a></li>
<li><a href="#xrdp">xrdp</a></li>
</ul>

<p>
Nog andere software maakt gebruik van cryptografische sleutels, maar is <a href="notvuln">niet kwetsbaar</a> omdat OpenSSL niet wordt gebruikt om sleutels te genereren of uit te wisselen.
</p>

<ul>
<li><a href="notvuln#ckermit">ckermit</a></li>
<li><a href="notvuln#gnupg">GnuPG</a></li>
<li><a href="notvuln#iceweasel">Iceweasel</a></li>
<li><a href="notvuln#mysql">MySQL</a></li>
</ul>

<p>
Voor instructies voor het overzetten van sleutels voor andere software, wilt u misschien de door gebruikers ingediende informatie raadplegen op <url https://wiki.debian.org/SSLkeys>
</p>

<h1><a name="openssh">OpenSSH</a></h1>

<p>
Er is een bijgewerkt pakket uitgebracht via
<a href="$(HOME)/security/2008/dsa-1576">DSA-1576</a>, dat sleutelomzetting vergemakkelijkt.
</p>

<p>1. Installeer de beveiligingsupdates uit DSA-1576</p>

   <p>Zodra de update is toegepast, worden zwakke gebruikerssleutels waar mogelijk automatisch geweigerd (hoewel ze niet in alle gevallen kunnen worden gedetecteerd).  Als u dergelijke sleutels gebruikt voor gebruikersauthenticatie, werken ze onmiddellijk niet meer en moeten ze worden vervangen (zie stap 3).</p>

   <p>OpenSSH-computersleutels kunnen automatisch worden geregenereerd wanneer de OpenSSH-beveiligingsupdate wordt toegepast. De update vraagt om bevestiging voordat deze stap wordt uitvoerd.</p>

<p>2. Werk de known_hosts-bestanden van OpenSSH bij</p>

   <p>Het vernieuwen van computersleutels veroorzaakt een waarschuwing bij het verbinden met het systeem via SSH totdat de computersleutel is bijgewerkt in het bestand known_hosts op de client. De waarschuwing ziet er als volgt uit:</p>

<pre>
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that the RSA host key has just been changed.
</pre>

<pre>
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@  WAARSCHUWING: IDENTIFICATIE EXTERNE COMPUTER IS VERANDERD! @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
HET IS MOGELIJK DAT IEMAND IETS SLECHTS DOET!
Iemand zou u op dit moment kunnen afluisteren (man-in-the-middle-aanval)!
Het is ook mogelijk dat de RSA-computersleutel zojuist is gewijzigd.
</pre>

   <p>In dit geval is de computersleutel gewoon gewijzigd, en moet u het relevante bestand known_hosts bijwerken zoals aangegeven in het waarschuwingsbericht.

   Het wordt aanbevolen een betrouwbaar kanaal te gebruiken om de serversleutel uit te wisselen.  Deze is te vinden in het bestand /etc/ssh/ssh_host_rsa_key.pub op de server; de vingerafdruk ervan kan worden afgedrukt met het commando:
</p>

      <p>ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub</p>

   <p>Naast de gebruikersspecifieke known_hosts-bestanden kan er ook een systeembreed bestand /etc/ssh/ssh_known_hosts bestaan. Dit bestand wordt zowel door de ssh-client als door sshd gebruikt voor de functionaliteit hosts.equiv.  Ook dit bestand moet worden bijgewerkt.
</p>

<p>3. Controleer alle OpenSSH-gebruikerssleutels</p>

   <p>De veiligste manier van handelen is om alle OpenSSH-gebruikerssleutels opnieuw te genereren, behalve wanneer met een voldoende hoge mate van zekerheid kan worden vastgesteld dat de sleutel werd gegenereerd op een niet-getroffen systeem.
</p>

   <p>Controleer of uw sleutel is aangetast door het hulpprogramma ssh-vulnkey uit te voeren, dat is opgenomen in de beveiligingsupdate. Standaard controleert ssh-vulnkey de standaardlocatie voor gebruikerssleutels (~/.ssh/id_rsa, ~/.ssh/id_dsa en ~/.ssh/identity), uw bestand authorized_keys (~/.ssh/authorized_keys en ~/.ssh/authorized_keys2), en de computersleutels van het systeem (/etc/ssh/ssh_host_dsa_key en /etc/ssh/ssh_host_rsa_key).
</p>

   <p>Om al uw eigen sleutels te controleren, ervan uitgaande dat ze zich op de standaardlocaties bevinden (~/.ssh/id_rsa, ~/.ssh/id_dsa of ~/.ssh/identity):
</p>

     <p>ssh-vulnkey</p>

   <p>Om alle sleutels op uw systeem te controleren:</p>

     <p>sudo ssh-vulnkey -a</p>

   <p>Om een sleutel op een niet-standaard locatie te controleren:</p>

     <p>ssh-vulnkey /path/to/key</p>

   <p>Als ssh-vulnkey zegt <q>Unknown (no blacklist information)</q> (Onbekend - geen informatie wat de zwarte lijst betreft), dan heeft het geen informatie over of die sleutel is aangetast. Vernietig in geval van twijfel de sleutel en maak een nieuwe aan.
   </p>

<p>4. Maak alle getroffen gebruikerssleutels opnieuw aan</p>

   <p>OpenSSH-sleutels die worden gebruikt voor gebruikersauthenticatie moeten handmatig opnieuw worden gegenereerd, inclusief sleutels die mogelijk naar een ander systeem zijn overgebracht nadat ze waren gegenereerd.
</p>

   <p>Nieuwe sleutels kunnen worden gegenereerd met ssh-keygen, bijv:</p>

   <pre>
   $ ssh-keygen
   Generating public/private rsa key pair.
   (Genereren van publiek/privaat rsa sleutelpaar.)
   Enter file in which to save the key (/home/user/.ssh/id_rsa):
   (Voer het bestand in waarin de sleutel moet worden opgeslagen (/home/user/.ssh/id_rsa):)
   Enter passphrase (empty for no passphrase):
   (Voer wachtwoordzin in (leeg voor geen wachtwoordzin):)
   Enter same passphrase again:
   (Voer dezelfde wachtwoordzin opnieuw in:)
   Your identification has been saved in /home/user/.ssh/id_rsa.
   (Uw identificatie werd opgeslagen in /home/user/.ssh/id_rsa.)
   Your public key has been saved in /home/user/.ssh/id_rsa.pub.
   (Uw publieke sleutel is opgeslagen in /home/user/.ssh/id_rsa.pub.)
   The key fingerprint is:
   (De vingerafdruk van de sleutel is:)
   00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00 user@host
   (00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00 gebruiker@computer)
   </pre>

<p>5. Werk de bestanden authorized_keys bij (indien nodig)</p>

   <p>Zodra de gebruikerssleutels zijn geregenereerd, moeten de relevante publieke sleutels worden doorgegeven aan alle authorized_keys-bestanden (en authorized_keys2-bestanden, indien van toepassing) op externe systemen.  Vergeet niet de aangetaste sleutel te verwijderen.
</p>

<h1><a name="openssl">OpenSSL: algemene instructies voor het genereren van PEM-sleutels</a></h1>

<p>
Dit is slechts een herinnering voor degenen die PEM-gecodeerde certificaten (her)genereren. Uw site heeft waarschijnlijk andere beleidsregels over het beheer van sleutels die u moet volgen. Bovendien moet u de certificaten mogelijk opnieuw laten ondertekenen door een externe certificeringsinstantie in plaats van een door uzelf ondertekend certificaat te gebruiken, zoals hieronder weergegeven:
</p>

<pre>
cd /etc/ssl/private
openssl genrsa 1024 &gt; mijnsite.pem
cd /etc/ssl/certs
openssl req -new -key ../private/mysite.pem -x509 -days 9999 -out mijnsite.pem
</pre>

<h1><a name="bincimap">bincimap</a></h1>

<p>
Het bincimap-pakket maakt automatisch een zelfondertekend certificaat aan via het openssl-programma voor de bincimap-ssl-service en plaatst dit in /etc/ssl/certs/imapd.pem. Voer het volgende uit om te regenereren:
</p>

<pre>
rm -f /etc/ssl/certs/imapd.pem
dpkg-reconfigure bincimap
</pre>

<h1><a name="boxbackup">boxbackup</a></h1>

<p>
Boxbackup is niet aanwezig in Debian stable, alleen in testing/Lenny
</p>

<p>
De bovenstroomse ontwikkelaar heeft een eerste impactanalyse gepubliceerd van sleutelmateriaal dat gemaakt werd op systemen met onvoldoende willekeurige PRNG. U kunt de details lezen
<a href="http://lists.warhead.org.uk/pipermail/boxbackup/2008-May/004476.html">op deze plaats</a>.
</p>

<p>
Als de PRNG in uw OpenSSL onvoldoende willekeurig was, moet u:
</p>

<ul>
<li>Alle getroffen certificaten die zijn gegenereerd of ondertekend op een getroffen systeem, opnieuw genereren</li>
<li>Alle gegevenssleutels (*-FileEncKeys.raw) opnieuw genereren</li>
<li>De gegevens op uw server op een passend beveiligingsniveau vernietigen (op zijn minst overschrijven met nullen, meer als u paranoïde bent)</li>
<li>Alles opnieuw uploaden</li>
<li>Passende maatregelen nemen, ervan uitgaande dat uw gegevens zonder authenticatie in platte tekst op een openbare server opgeslagen geweest zijn. (d.w.z. helemaal opnieuw beginnen, alle sporen van de back-upgegevens vernietigen en andere maatregelen nemen om het blootgeven van uw vertrouwelijke gegevens te verminderen)</li>
</ul>

<h1><a name="cryptsetup">cryptsetup</a></h1>

<p>
Cryptsetup zelf gebruikt geen openssl voor encryptie (dit geldt zowel voor apparaten met LUKS als met dm-crypt).
</p>

<p>
<em>Als</em> cryptsetup is geconfigureerd om met SSL geëncrypteerde sleutelbestanden te gebruiken (een niet-standaard instelling die expliciet door de gebruiker moet worden geconfigureerd) en er een defecte versie van openssl is gebruikt om het sleutelbestand te genereren, kan de encryptie van het sleutelbestand zwakker zijn dan verwacht (aangezien de salt / het zout  niet echt willekeurig is).
</p>

<p>
De oplossing is om het sleutelbestand opnieuw te encrypteren (als u er redelijk zeker van bent dat de geëncrypteerde sleutel niet aan derden is vrijgegeven) of om de getroffen partitie(s) te wissen en opnieuw te installeren met een nieuwe sleutel.
</p>

<p>
Instructies voor het opnieuw encrypteren van een sleutelbestand:
</p>

<p>
Doe het volgende voor elk met SSL geëncrypteerd sleutelbestand, en vervang &lt;ssl_encrypted_key_path&gt; door het pad naar het eigenlijke sleutelbestand:
</p>

<pre>
tmpkey=\$(tempfile)
openssl enc -aes-256-cbc -d -salt -in &lt;ssl_encrypted_key_path&gt; -out "$tmpkey"
shred -uz &lt;ssl_encrypted_key_path&gt;
openssl enc -aes-256-cbc -e -salt -in "$tmpkey" -out &lt;ssl_encrypted_key_path&gt;
shred -uz "$tmpkey"
</pre>

<h1><a name="dropbear">dropbear</a></h1>

<p>
Als u sleutels heeft van het type /etc/ssh/*host*, verwijder deze dan, of volg eerst de <a href="#openssh">instructies voor openssh</a> voordat u de sleutels van dropbear bijwerkt.
</p>

<p>
Het script postinst van Dropbear converteert bestaande openssh-sleutels naar de door dropbear gebruikte indeling als het de computersleutels van dropbear niet kan vinden.
</p>

<pre>
rm /etc/dropbear/*_host_key
dpkg-reconfigure dropbear
</pre>

<p>
Merk op dat sleutels die zijn gegenereerd door Dropbear zelf niet kwetsbaar zijn (het gebruikt libtomcrypt in plaats van OpenSSL).
</p>

<h1><a name="ekg">ekg</a></h1>

<p>
Gebruikers van programma's ekg en ekg2 (het laatste is alleen aanwezig in experimental) welke gebruik maken van de encryptiefunctionaliteit <q>SIM</q>, die een sleutelpaar hebben gegenereerd met behulp van het commando <q>/key [-g|--generate]</q> (dat libssl gebruikt om het werk te doen) zouden de sleutels opnieuw moeten genereren na het upgraden van libssl en het herstarten van het programma.
</p>

<p>
De bovenstroomse ontwikkelaars hebben een bericht op hun website geplaatst:
<a href="http://ekg.chmurka.net/index.php">http://ekg.chmurka.net/index.php</a>
</p>

<p>
Als u meer hulp nodig heeft, vraag die dan op ekg-users@lists.ziew.org of ekg2-users@lists.ziew.org (zowel Engels als Pools).
</p>

<h1><a name="ftpd-ssl">ftpd-ssl</a></h1>

<pre>
rm -f /etc/ftpd-ssl/ftpd.pem /etc/ssl/certs/ftpd.pem
dpkg-reconfigure ftpd-ssl
</pre>

<p>
Dit geldt voor de standaardinstelling. Als de lokale beheerder verdere SSL-infrastructuur heeft opgezet, moeten deze sleutels ook opnieuw worden gegenereerd.
</p>

<h1><a name="gforge">gforge</a></h1>

<p>
Het pakket gforge-web-apache2 in sid en lenny zet de website op met een dummycertificaat als er geen bestaand certificaat wordt gevonden. Gebruikers worden dan aangemoedigd om het te vervangen door een <q>echt</q> exemplaar. Het dummy-certificaat in kwestie is het Snake Oil-certificaat, dus het zou al bekend moeten staan als een zwak certificaat (zelfs zonder de SSL-bug), maar sommige gebruikers accepteren het misschien zonder erbij na te denken.
</p>

<h1><a name="kerberos">MIT Kerberos (krb5)</a></h1>

<p>
Geen enkel onderdeel van MIT Kerberos in Debian 4.0 (<q>Etch</q>) maakt gebruik van OpenSSL, en dus is Kerberos in Debian 4.0 helemaal niet aangetast.
</p>

<p>
In Lenny maakt het aparte binaire pakket krb5-pkinit gebruik van OpenSSL. MIT Kerberos zelf genereert geen sleutelparen, zelfs niet wanneer de plug-in PKINIT wordt gebruikt, dus eventuele kwetsbare sleutelparen voor de lange termijn zouden buiten de software van MIT Kerberos om zijn gegenereerd. De plug-in PKINIT verwijst alleen naar bestaande sleutelparen en is niet verantwoordelijk voor sleutelbeheer.
</p>
<p>
Sleutelparen voor de lange termijn die gebruikt worden met PKINIT kunnen aangetast zijn als ze werden aangemaakt op een aangetast Debian-systeem, maar een dergelijke aanmaak gebeurt buiten MIT Kerberos om.
</p>
<p>
De willekeurige sleutelfuncties van OpenSSL worden echter gebruikt voor de DH-uitwisseling wanneer PKINIT-authenticatie wordt gebruikt, wat betekent dat een aanvaller brute kracht kan gebruiken om toegang te krijgen tot het KDC-antwoord op een PKINIT-authenticatie en vervolgens toegang kan krijgen tot alle sessies die zijn aangemaakt met behulp van servicetickets van die authenticatie.
</p>
<p>
Alle KDC's die de plug-in PKINIT van Lenny gebruiken, moeten hun pakket libssl0.9.8 onmiddellijk laten upgraden en de Kerberos KDC herstarten met:
</p>
<pre>
/etc/init.d/krb5-kdc restart
</pre>
<p>
Alle Kerberos ticket-granting tickets (TGT's) of versleutelde sessies die het resultaat zijn van PKINIT-authenticatie met behulp van een Kerberos KDC met het aangetaste libssl moeten als verdacht worden behandeld; het is mogelijk dat aanvallers met pakketafvangeers deze sleutels en sessies kunnen compromitteren.
</p>

<h1><a name="nessus">Nessus</a></h1>

<p>Het post-installatiescript van het Nessus-serverpakket (nessusd) maakt enkele SSL-sleutels aan voor veilige communicatie tussen een Nessus-server en client. Dat communicatiekanaal moet als gecompromitteerd worden beschouwd, aangezien een malafide gebruiker in staat zou kunnen zijn om de informatie die wordt uitgewisseld tussen de server en de client (inclusief informatie over kwetsbaarheden op externe computers) te onderscheppen en mogelijk opdrachten naar de servers te sturen als de ingelogde gebruiker.
</p>

<p>Bovendien, als de beheerder de Nessus CA-sleutel of een gebruikerscertificaat (met nessus-mkcert-client) heeft gemaakt voor externe authenticatie op een server waarop de OpenSSL-versie was geïnstalleerd waarop dit beveiligingsprobleem betrekking heeft, moeten die sleutels als gecompromitteerd worden beschouwd. Merk op dat externe gebruikers met toegang tot de Nessus-server aanvallen kunnen lanceren op de servers die ze mogen aanvallen en, indien ingeschakeld in de lokale configuratie (in Debian is dit standaard <q>no</q>), plug-ins kunnen uploaden die zouden worden uitgevoerd op de Nessus-server met systeembeheerdersrechten.
</p>

<p>De configuratiescripts van de pakketbeheerder zullen de OpenSSL-certificaten opnieuw genereren wanneer dit geconfigureerd is wanneer deze niet kunnen worden gevonden. U moet de certificaten verwijderen en nieuwe laten genereren met:</p>

<pre>
rm -f /var/lib/nessus/CA/cacert.pem
rm -f /var/lib/nessus/CA/servercert.pem
rm -f /var/lib/nessus/private/CA/cakey.pem
rm -f /var/lib/nessus/private/CA/serverkey.pem
dpkg-reconfigure nessusd
</pre>

<p>Zodra dit gebeurd is moet u de oude gebruikerssleutels verwijderen in /var/lib/nessus/users/GEBRUIKERSNAAM/auth en ze opnieuw genereren met nessus-mkcert-client. Oude sleutels zijn ongeldig zodra de CA-sleutel is verwijderd.
</p>

<p>Nadat de CA-sleutel opnieuw is gegenereerd, moet u ook het nieuwe CA-certificaat naar uw gebruikers verspreiden, en uw gebruikers moeten het nieuwe certificaat van de Nessus-server accepteren zodra ze opnieuw verbinding maken. Certificaatinstellingen voor de oude server zouden automatisch moeten worden verwijderd, maar u kunt ze ook handmatig verwijderen door <code>.nessusrc.cert</code> (bij gebruik van de Nessus-client) of <code>.openvasrc.cert</code> (bij gebruik van de OpenVAS-client) te bewerken..
</p>


<h1><a name="openswan">OpenSWAN / StrongSWAN</a></h1>

<pre>
rm /etc/ipsec.d/private/`hostname`Key.pem /etc/ipsec.d/certs/`hostname`Cert.pem
dpkg-reconfigure (open|strong)swan
/etc/init.d/ipsec restart
</pre>

<p>
Pas op: Het herstarten van de ipsec-diensten beëindigt alle momenteel openstaande IPSec-verbindingen, die mogelijk opnieuw gestart moeten worden vanaf de andere kant.
</p>

<h1><a name="openvpn">OpenVPN</a></h1>

<p>
Maak een back-up van uw geheime sleutelbestanden. Hoewel sleutelnamen willekeurig zijn, kunnen ze worden gedetecteerd door het uitvoeren van
</p>
<pre>grep secret /etc/openvpn/*.conf</pre>

<p>
Maak ze opnieuw aan met
</p>
<pre>openvpn --genkey --secret BESTANDSNAAM_GEHEIME_SLEUTELS</pre>

<p>
Kopieer vervolgens de gedeelde geheime sleutels naar de externe computers en herstart de VPN op elke computer met
</p>
<pre>/etc/init.d/openvpn force-reload</pre>

<h1><a name="proftpd">Proftpd</a></h1>

<p>
De verpakking van dit programma in Debian voorziet geen aanmaak van sleutels, dus de volgende stappen zijn alleen nodig als er extern SSL-sleutels zijn aangemaakt.
</p>

<p>
Een toekomsitge upload van proftpd naar unstable zal een tls.conf-sjabloon bevatten met onderstaande opmerking.
</p>

<p>
Merk op dat het genereren van een zelfondertekend certificaat een beetje afwijkt van wat in de algemene openssl sectie wordt voorgesteld, om het gebruik van een expliciet wachtwoord bij het opstarten van de achtergronddienst te vermijden.
</p>

<p>
U kunt een zelfondertekend certificaat (opnieuw) genereren met een commando als:
</p>
<pre>
 openssl req -x509 -newkey rsa:1024 \
         -keyout /etc/ssl/private/proftpd.key -out /etc/ssl/certs/proftpd.crt \
         -nodes -days 365
</pre>

<p>
Beide bestanden mogen alleen door root gelezen kunnen worden. De bestandspaden kunnen worden gecontroleerd/geconfigureerd via de volgende configuratierichtlijnen:
</p>
<pre>
TLSRSACertificateFile                   /etc/ssl/certs/proftpd.crt
TLSRSACertificateKeyFile                /etc/ssl/private/proftpd.key
TLSCACertificateFile                    /etc/ssl/certs/CA.pem
TLSOptions                              NoCertRequest
</pre>

<h1><a name="puppet">puppet</a></h1>

<p>
Er zijn twee methoden om met puppet-certificaten om te gaan, de ene is via capistrano, de tweede is handmatig.
</p>

<p>
Het opnieuw genereren van Puppet SSL-Certificaten met behulp van capistrano wordt hier beschreven:
<a href="http://reductivelabs.com/trac/puppet/wiki/RegenerateSSL">http://reductivelabs.com/trac/puppet/wiki/RegenerateSSL</a>
</p>

<p>
De handmatige stappen zijn de volgende:
</p>

<ol>
<li>U moet uw CA-info wissen en opnieuw genereren:
<pre>
/etc/init.d/puppetmaster stop
rm -rf $vardir/ssl/*
/etc/init.d/puppetmaster start
</pre>
<p>
Als u echter mongrel gebruikt, moet u in plaats van puppetmaster te starten vanuit het init-script, eerst het frontend programma dat op het web luistert (apache, nginx, enz.) stoppen en dan het volgende doen:
</p>
<pre>
puppetmasterd --daemonize ; sleep 30 ; pkill -f 'ruby /usr/sbin/puppetmasterd'
</pre>
<p>
Het bovenstaande is nodig omdat om de een of andere reden puppetmaster zijn CA niet regenereert wanneer het met mongrel werkt.
</p>
</li>
<li>Wis alle clientcertificaten
<pre>
/etc/init.d/puppet stop
rm $vardir/ssl/*
</pre>
</li>
<li>Laat elke client een nieuw certificaat aanvragen:
<pre>
puppetd --onetime --debug --ignorecache --no-daemonize
</pre>
</li>
<li>Zodra alle aanvragen binnen zijn, kunt u ze allemaal tegelijk ondertekenen:
<pre>
puppetca --sign --all
</pre>
</li>
<li>Start uw puppet-clients op:
<pre>
/etc/init.d/puppet start
</pre>
</li>
</ol>

<p>
U zou ook tijdelijk automatisch ondertekenen kunnen inschakelen, als u dat goed vindt.
</p>

<h1><a name="sendmail">sendmail</a></h1>

<p>
Sendmail (zowel in Etch als in Lenny) maakt facultatief standaard OpenSSL-certificaten aan bij de installatie.
</p>

<p>
De procedure voor het omzetten van de sleutels is triviaal:
</p>
<pre>
/usr/share/sendmail/update_tls new
</pre>

<p>
Als u de sjablonen in /etc/mail/tls hebt aangepast, zullen die waarden opnieuw worden gebruikt om de nieuwe certificaten te maken.
</p>

<h1><a name="ssl-cert">ssl-cert</a></h1>

<p>
Het snakeoil-certificaat /etc/ssl/certs/ssl-cert-snakeoil.pem kan opnieuw worden gemaakt met:
</p>

<pre>make-ssl-cert generate-default-snakeoil --force-overwrite</pre>

<h1><a name="telnetd-ssl">telnetd-ssl</a></h1>

<pre>
rm -f /etc/telnetd-ssl/telnetd.pem /etc/ssl/certs/telnetd.pem
dpkg-reconfigure telnetd-ssl
</pre>

<p>
Dit geldt voor de standaardopstelling. Als de lokale beheerder verdere SSL-infrastructuur heeft opgezet, moeten deze sleutels ook opnieuw worden gegenereerd.
</p>

<h1><a name="tinc">tinc</a></h1>

<p>
Verwijder alle verdachte bestanden met openbare en private sleutels:
</p>
<ol>
<li>Verwijder rsa_key.priv.</li>
<li>Bewerk alle bestanden in de map hosts/ en verwijder de blokken met openbare sleutels.</li>
</ol>

<p>
Genereer een nieuw publiek/privaat sleutelpaar:
</p>
<pre>
tincd -n &lt;netname&gt; -K
</pre>

<p>
Wissel het configuratiebestand op uw computer met de nieuwe publieke sleutel uit met andere leden van uw VPN. Vergeet niet uw tinc-achtergronddiensten te herstarten.
</p>

<h1><a name="tor">tor</a></h1>

<p>
Tor is niet in stable, maar aangetast in Lenny.
</p>

<p>
Clients die versie 0.1.2.x gebruiken, zijn niet aangetast. Elke versie van Tor-knooppunten of verborgen dienstverleners, alsook iedereen die versie 0.2.0.x gebruikt, is wel aangetast.
</p>

<p>
Raadpleeg de
<a href="http://archives.seul.org/or/announce/May-2008/msg00000.html">aankondiging van de kwetsbaarheid</a> in de mailinglijst met Tor-aankondigingen.
</p>

<p>
Het wordt aanbevolen om op te waarderen naar versie 0.1.2.19-3 (beschikbaar in testing, unstable, backports.org en in
de gebruikelijke <a
href="https://wiki.torproject.org/noreply/TheOnionRouter/TorOnDebian">noreply.org-pakketbron</a>) of versie 0.2.0.26-rc-1 (beschikbaar in experimental en in de gebruikelijke <a
href="https://wiki.torproject.org/noreply/TheOnionRouter/TorOnDebian">noreply.org-pakketbron</a>). Als u een relais bent, dwingen deze versies het genereren van nieuwe serversleutels (/var/lib/tor/keys/secret_*) af.
</p>

<p>
Als u een Tor-knooppunt bent zonder gebruik te maken van de infrastructuur van het pakket (gebruiker debian-tor, /var/lib/tor als DataDirectory, enz.), moet u handmatig slechte sleutels verwijderen. Zie de hierboven geplaatste link naar het advies.
</p>

<p>
Als u een verborgen dienstverlener bent, en uw sleutel in de betrokken periode hebt aangemaakt met een slechte libssl, verwijder dan alstublieft de privésleutel van uw verborgen dienst. Dit zal de computernaam van uw verborgen dienst veranderen en het kan nodig zijn uw servers opnieuw te configureren.
</p>

<p>
Indien u versie 0.2.0.x gebruikt, is een upgrade ten stelligste aanbevolen &mdash; 3 van de 6 v3 autoriteitsservers hebben gecompromitteerde sleutels.  Oude 0.2.0.x-versies zullen over een tweetal weken stoppen met werken.
</p>

<h1><a name="xrdp">xrdp</a></h1>

<p>
xrdp gebruikt gegenereerde sleutels. De meeste clients controleren die sleutels niet standaard, daarom is het veranderen ervan pijnloos. U hoeft alleen maar het volgende te doen:
</p>

<pre>
rm /etc/xrdp/rsakeys.ini
/etc/init.d/xrdp restart
</pre>

<p>
xrdp is niet in stable.
</p>
