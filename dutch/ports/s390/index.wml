#use wml::debian::template title="Debian voor S/390"
#use wml::debian::toc
#use wml::debian::translation-check translation="0d2ff40f3ac99634f3bb83bb7589af752e6783c5"

<toc-display/>

<toc-add-entry name="status">Status</toc-add-entry>

<p>S/390 is een officieel door Debian ondersteunde architectuur geweest
sinds de release van Debian 3.0 (woody).</p>

<p>Raadpleeg de
<a href="$(HOME)/releases/stable/s390x/">Installatiehandleiding</a> voor
instructies voor de installatie van Debian.</p>

<toc-add-entry name="team">Het team dat Debian geschikt maakt voor S/390</toc-add-entry>

<p>
De volgende mensen hebben bijgedragen tot het geschikt maken van Debian voor de s390:
</p>

<ul>
  <li>Aurélien Jarno</li>
  <li>Bastian Blank</li>
  <li>Chu-yeon Park and Jae-hwa Park</li>
  <li>Frank Kirschner</li>
  <li>Frans Pop</li>
  <li>Gerhard Tonn</li>
  <li>Jochen Röhrig</li>
  <li>Matt Zimmerman</li>
  <li>Philipp Kern</li>
  <li>Richard Higson</li>
  <li>Stefan Gybas</li>
</ul>

<toc-add-entry name="development">Ontwikkeling</toc-add-entry>

<p>Een build-server en de porterbox voor de <em>s390x</em>-architecturen
worden momenteel gehost door <a href="https://www.itzbund.de/">Informationstechnikzentrum
Bund (ITZBund)</a>. Andere build-servers worden ter beschikking gesteld door
het <a href="http://www.iic.kit.edu">Informatics Innovation Center,
Karlsruhe Institute of Technology (KIT)</a> en het <a
href="https://www.marist.edu/">Marist College</a>. We danken deze
host-dienstverleners voor hun steun!</p>

<p>In het verleden hebben <a href="http://www.millenux.de/">Millenux</a> en het <a
href="https://www.ibm.com/it-infrastructure/z/os/linux-support">Linux
Community Development System</a> dergelijke build-machines gehost.</p>

<toc-add-entry name="contact">Contactinformatie</toc-add-entry>

<p>Indien u wenst te helpen, moet u intekenen op de mailinglijst debian-s390. Daarvoor moet u een bericht, met als onderwerp het woord "subscribe",
sturen naar <email "debian-s390-request@lists.debian.org">, of u kunt de
<a href="https://lists.debian.org/debian-s390/">webpagina van de mailinglijst</a> gebruiken.
U kunt de
<a href="https://lists.debian.org/debian-s390/">mailinglijstarchieven</a> ook doorbladeren en doorzoeken.</p>
