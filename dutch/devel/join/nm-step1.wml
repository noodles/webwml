#use wml::debian::template title="Stap 1: Aanmelding" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="0f3581ea8ff22d12b1665c2fb885ea6ccfe11f1c"

<p>De informatie op deze pagina, hoewel openbaar, zal voornamelijk van belang
zijn voor toekomstige Debian-ontwikkelaars.</p>

<h2>Step 1: Aanmelding</h2>

<p>Alvorens zich aan te melden, moeten kandidaat-ontwikkelaars nagaan of zij
voorbereid zijn op alle onderdelen van de controles. Om dit zo gemakkelijk
mogelijk te maken, volgen hieronder de belangrijkste voorwaarden:</p>

<ul>
 <li><h4>De kandidaat moet het eens zijn met de filosofie van Debian</h4>
  <p>Kandidaten moeten het
   <a href="$(HOME)/social_contract">Sociaal Contract</a> en de
   <a href="$(HOME)/social_contract#guidelines">Debian-richtlijnen
   voor vrije software</a> gelezen hebben en ermee akkoord gaan om ze
   na te leven in hun met Debian verband houdende werkzaamheden.
  </p>
 </li>

 <li><h4>De identiteit van de kandidaat moet worden geverifieerd</h4>
  <p>Dit wordt gedaan door op de OpenPGP-sleutel van de kandidaat ten minste
   twee handtekeningen te hebben van Debian-ontwikkelaars. Het is ook sterk
   aanbevolen om meer handtekeningen te hebben van mensen die goed ingebed
   zijn in het web van vertrouwen.
  </p>

  <p>Indien de locatie van de kandidaat het <em>onmogelijk</em> maakt om een
   sleutel te laten ondertekenen door een andere Debian-ontwikkelaar, kan ook
   een met zijn GPG-sleutel ondertekende ingescande foto van zijn rijbewijs of
   paspoort geaccepteerd worden als een alternatieve identificatiemethode.
  </p>
 </li>

 <li><h4>De kandidaat moet in staat zijn om zijn taken als ontwikkelaar uit te
  voeren</h4>
  <p>Dit betekent dat de kandidaat ervaring moet hebben met het verpakken en
  onderhouden van pakketten, of met documenteren of vertalen, afhankelijk van
  het domein waarin hij/zij actief wil zijn.
  </p>

  <p>Het is aanbevolen dat de kandidaat een
  <a href="newmaint#Sponsor">sponsor</a> krijgt om hem te helpen dit te
  bereiken.</p>
 </li>

 <li><h4>De kandidaat moet de documentatie voor ontwikkelaars gelezen hebben</h4>
  <p>Later in het proces zal de <a href="newmaint#AppMan">aanmeldingsbeheerder</a>
  de kennis van de kandidaat testen over concepten die
  beschreven worden in
   <a href="$(DOC)/debian-policy/">Het beleidshandboek van Debian</a>,
   <a href="$(DOC)/developers-reference/">De referentiehandleiding voor
   ontwikkelaars</a>,
   <a href="$(DOC)/maint-guide/">De handleiding voor nieuwe ontwikkelaars</a>,
   enz.
  </p>
 </li>

 <li><h4>De kandidaat moet voldoende tijd ter beschikking hebben</h4>
  <p>Het afwerken van de controles voor nieuwe leden en een Debian-ontwikkelaar
  zijn vragen een regelmatige tijdsinvestering. Het onderhouden van pakketten
  uit het hoofdarchief is een belangrijk engagement en kan behoorlijk wat tijd
  kosten.
  </p>
 </li>

 <li><h4>De kandidaat moet een <a href="newmaint#Advocate">\
  pleitbezorger</a> hebben</h4>
  <p>Om een ontwikkelaar te overtuigen een pleitbezorger te worden voor
  hem/haar, moet de kandidaat betrokken zijn bij de ontwikkeling van
  Debian &ndash; open bugs tegen bestaande pakketten helpen aanpakken, een
  verweesd pakket adopteren, werken aan het installatiesysteem, nuttige nieuwe
  software verpakken, documentatie schrijven of bijwerken, enz.
  </p>
 </li>
</ul>

<p>Zodra de kandidaat aan de bovenstaande vereisten voldoet, kan deze zijn
<a href="https://nm.debian.org/public/newnm">aanmelding als kandidaat-lid</a> indienen.</p>

<p>Na ontvangst van de aanmelding, wordt deze beheerd door de
<a href="./newmaint#FrontDesk">Frontdesk</a>. Die zal, van zodra een persoon
beschikbaar is (wat enkele weken kan duren), deze aanstellen als de
<a href="./newmaint#AppMan">Aanmeldingsbeheerder</a> (AM) van de
<a href="./newmaint#Applicant">Kandidaat</a> (d.w.z. van u).</p>

<p>Alle verdere communicatie moet plaatsvinden tussen de kandidaat en zijn/haar
aanmeldingsbeheerder, maar als er problemen ontstaan, is de frontdesk het
eerste aanspreekpunt.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
