#use wml::debian::template title="Licentie-informatie" GEN_TIME="yes"
#use wml::debian::translation-check translation="cc23650ce5b1cc4293f9f5440623cd71342482af"

<p>Deze pagina geeft de mening weer van sommige personen die bijdragen aan
debian-legal, over de wijze waarop bepaalde licenties beantwoorden aan de
<a href="$(HOME)/social_contract#guidelines">Debian Free Software
Guidelines</a> (DFSG - De Debian richtlijnen inzake vrije software).
De meeste van deze meningen werden gevormd via discussies op de
<a href="https://lists.debian.org/debian-legal/">mailinglijst debian-legal</a>
in antwoord op vragen van kandidaat-pakketbeheerders of licentiegevers.
We verwelkomen vragen van beheerders die bepaalde licenties overwegen,
maar we moedigen de meeste beheerders aan om een van de algemene licenties
te gebruiken: GPL, LGPL, aangepaste BSD of Artistic.</p>

<p>Software die voor Debian verpakt wordt, wordt normaal geklasseerd in
een van de volgende vier categorieën. Er is vrije software (main),
niet-vrije software en firmware (non-free, non-free-firmware), vrije software die bepaalde niet-vrije
software vereist (contrib) en software die niet verspreid kan worden
(wordt niet opgenomen).
<a href="$(DOC)/debian-policy/ch-archive.html">Afdeling 2 in het Debian beleidshandboek</a>
legt precies uit hoe de DFSG toegepast worden op het archief. Bij
twijfel over licenties worden pakketbeheerders gevraagd om hierover
debian-legal te e-mailen met in de inhoud van het e-mailbericht de tekst van
een eventuele nieuwe licentie. Misschien vindt u het nuttig om
<a href="https://lists.debian.org/search.html">de archieven van de
mailinglijst</a> te doorzoeken op de benaming van de licentie vooraleer
e-mails met vragen over de licentie naar mailinglijsten te sturen.
Indien u toch vragen mailt, verwijs dan naar bepaalde vroegere relevante
besprekingen.</p>

<p>debian-legal heeft adviserende bevoegdheid. De feitelijke besluitvormers
zijn de ftpmasters en de pakketbeheerders. Als men echter de meeste van de
doorgaans ruimdenkende medewerkers op debian-legal niet kan overtuigen, is het
waarschijnlijk niet duidelijk dat de software de DFSG volgt.</p>

<p>Aangezien de ftpmasters en de pakketbeheerders de feitelijke besluitvormers
zijn, is het een heel goed idee om
<a href="https://ftp-master.debian.org/REJECT-FAQ.html">de ftpmasters REJECT
FAQ</a> te raadplegen en site:packages.debian.org te doorzoeken naar eventuele
licenties waarover u twijfels heeft, op zoek naar andere voorbeelden van de
wijze waarop men hiermee omgaat voor Debian. (De zoekactie werkt, omdat
copyright-bestanden van pakketten als platte tekst gepubliceerd worden op
packages.debian.org).</p>

<p>Andere lijsten worden beheerd door de
<a href="https://www.gnu.org/licenses/license-list">Free Software
Foundation</a> (FSF) en het
<a href="https://opensource.org/licenses/">Open Source
Initiative</a> (OSI). Merk evenwel op dat het Debian-project beslist
over specifieke pakketten, eerder dan over licenties in abstracto, en dat
in deze lijsten algemene toelichtingen aan bod komen. Het is mogelijk dat
een pakket software onder een "vrije" licentie bevat, maar dat een ander
aspect het niet-vrij maakt. Soms wordt op debian-legal in abstracto
commentaar gegeven over een licentie zonder de toepassing te maken op
specifieke software. Hoewel een dergelijke bespreking kan duiden op mogelijke
problemen, is het vaak niet mogelijk om tot duidelijke antwoorden te komen
totdat het onderzoek gevoerd wordt op specifieke software.</p>

<p>U kunt contact opnemen met debian-legal indien u vragen heeft over of
commentaar bij deze samenvattingen.</p>

<p>Momenteel in Debian main aangetroffen licenties zijn:</p>

<ul>
<li><a href="https://www.gnu.org/licenses/gpl.html">GNU General Public licentie</a> (vaak voorkomend)</li>
<li><a href="https://www.gnu.org/licenses/lgpl.html">GNU Lesser General Public licentie</a> (vaak voorkomend)</li>
<li>GNU Library General Public licentie (vaak voorkomend)</li>
<li><a href="https://opensource.org/licenses/BSD-3-Clause">Aangepaste BSD-licentie</a> (vaak voorkomend)</li>
<li><a href="http://www.perl.com/pub/a/language/misc/Artistic.html">Perl Artistic licentie</a> (vaak voorkomend)</li>
<li><a href="http://www.apache.org/licenses/">Apache-licentie</a></li>
<li><a href="http://www.jclark.com/xml/copying.txt">Licenties in Expat/MIT-stijl</a></li>
<li><a href="https://zlib.net/zlib/zlib_license.html">Licenties in zlib-stijl</a></li>
<li><a href="http://www.latex-project.org/lppl/">LaTeX Project publieke licentie</a></li>
<li><a href="http://www.python.org/download/releases/2.5.2/license/">Python Software Foundation licentie</a></li>
<li><a href="http://www.ruby-lang.org/en/LICENSE.txt">Ruby's licentie</a></li>
<li><a href="http://www.php.net/license/">PHP-licentie</a></li>
<li><a href="http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231">W3C Software verklaring en licentie</a></li>
<li><a href="http://www.openssl.org/source/license.html">OpenSSL-licentie</a></li>
<li><a href="https://opensource.org/licenses/Sleepycat">Sleepycat-licentie</a></li>
<li><a href="http://www.cups.org/book/examples/LICENSE.txt">Common UNIX Printing System licentieovereenkomst</a></li>
<li>vhf Public licentie</li>
<li><a href="http://tunes.org/legalese/bugroff.html">"No problem Bugroff" licentie</a></li>
<li>Ongewijzigde BSD-licentie (ook gekend als de
originele of uit 4 clausules bestaande BSD-licentie.
Deze bevatte een aankondigingsvereiste en wordt nu zelfs door het
BSD-project als verouderd beschouwd.)</li>
<li>publiek domein (strikt genomen geen licentie)</li>
<li><a href="http://www.openafs.org/frameset/dl/license10.html">IBM publieke licentie versie 1.0</a></li>
</ul>

<p>Indien u één van deze licenties gebruikt, tracht dan de laatste versie
te gebruiken en maak enkel de noodzakelijke aanpassingen, tenzij anders
aangegeven.
Licenties die als (vaak voorkomend) gemarkeerd zijn, kan u op een Debian
systeem vinden in <tt>/usr/share/common-licenses</tt>.</p>

<p>Momenteel in de non-free afdeling van het archief aangetroffen licenties
zijn:</p>

<ul>
<li>NVIDIA softwarelicentie</li>
<li>SCILAB-licentie</li>
<li>Limited Use softwarelicentieovereenkomst</li>
<li>Non-Commercial licentie</li>
<li>FastCGI / Open Market licentie</li>
<li>LaTeX2HTML-licentie</li>
<li>Open Publication licentie</li>
<li>Free Document Dissemination licentie</li>
<li>AT&amp;T Open Source licentie</li>
<li>Apple Public Source licentie</li>
<li>Aladdin Free Public licentie</li>
<li>Generic amiwm licentie (een licentie in XV-stijl)</li>
<li>Digital License Agreement</li>
<li>Moria/Angband licentie</li>
<li>Unarj licentie</li>
<li>id softwarelicentie</li>
<li>qmail terms</li>
</ul>

<p>Upload software onder deze licenties niet naar het archief main.</p>

<p>Daarnaast kan bepaalde software helemaal niet verspreid worden, zelfs niet
in non-free (bijvoorbeeld omdat ze helemaal geen licentie bevat).</p>


<h2>Werk in uitvoering</h2>

<p>Voor hulp bij het interpreteren van de DFSG moet u de
<a href="https://people.debian.org/~bap/dfsg-faq">DFSG FAQ</a>
raadplegen. U vindt er antwoorden op een aantal algemene vragen
over de DFSG en over hoe u software moet analyseren.</p>

