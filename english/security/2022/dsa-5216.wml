<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Nick Wellnhofer discovered that the xsltApplyTemplates function in
libxslt, an XSLT processing runtime library, is prone to a
use-after-free flaw, resulting in a denial of service, or potentially
the execution of arbitrary code if a specially crafted file is
processed.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.1.34-4+deb11u1.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>For the detailed security status of libxslt please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxslt">\
https://security-tracker.debian.org/tracker/libxslt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5216.data"
# $Id: $
