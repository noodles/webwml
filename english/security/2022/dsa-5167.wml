<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Matthias Gerstner discovered that the --join option of Firejail,
a sandbox to restrict an application environment, was susceptible
to local privilege escalation to root.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 0.9.58.2-2+deb10u3.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 0.9.64.4-2+deb11u1.</p>

<p>We recommend that you upgrade your firejail packages.</p>

<p>For the detailed security status of firejail please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firejail">\
https://security-tracker.debian.org/tracker/firejail</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5167.data"
# $Id: $
