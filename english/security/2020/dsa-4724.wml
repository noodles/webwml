<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9802">CVE-2020-9802</a>

    <p>Samuel Gross discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9803">CVE-2020-9803</a>

    <p>Wen Xu discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9805">CVE-2020-9805</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to universal cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9806">CVE-2020-9806</a>

    <p>Wen Xu discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9807">CVE-2020-9807</a>

    <p>Wen Xu discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9843">CVE-2020-9843</a>

    <p>Ryan Pickren discovered that processing maliciously crafted web
    content may lead to a cross site scripting attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9850">CVE-2020-9850</a>

    <p>@jinmo123, @setuid0x0_, and @insu_yun_en discovered that a remote
    attacker may be able to cause arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13753">CVE-2020-13753</a>

    <p>Milan Crha discovered that an attacker may be able to execute
    commands outside the bubblewrap sandbox.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.28.3-2~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4724.data"
# $Id: $
