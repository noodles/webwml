<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1788">CVE-2021-1788</a>

    <p>Francisco Alonso discovered that processing maliciously crafted
    web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1844">CVE-2021-1844</a>

    <p>Clement Lecigne and Alison Huffman discovered that processing
    maliciously crafted web content may lead to arbitrary code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1871">CVE-2021-1871</a>

    <p>An anonymous researcher discovered that a remote attacker may be
    able to cause arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.32.1-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4923.data"
# $Id: $
