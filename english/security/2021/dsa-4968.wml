<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Ori Hollander reported that missing header name length checks in the
htx_add_header() and htx_add_trailer() functions in HAProxy, a fast and
reliable load balancing reverse proxy, could result in request smuggling
attacks or response splitting attacks.</p>

<p>Additionally this update addresses #993303 introduced in DSA 4960-1
causing HAProxy to fail serving URLs with HTTP/2 containing '//'.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.2.9-2+deb11u2.</p>

<p>We recommend that you upgrade your haproxy packages.</p>

<p>For the detailed security status of haproxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/haproxy">\
https://security-tracker.debian.org/tracker/haproxy</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4968.data"
# $Id: $
