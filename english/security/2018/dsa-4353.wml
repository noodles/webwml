<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were found in PHP, a widely-used open source
general purpose scripting language: The EXIF module was susceptible to
denial of service/information disclosure when parsing malformed images,
the Apache module allowed cross-site-scripting via the body of a
"Transfer-Encoding: chunked" request and the IMAP extension performed
insufficient input validation which can result in the execution of
arbitrary shell commands in the imap_open() function and denial of
service in the imap_mail() function.</p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 7.0.33-0+deb9u1.</p>

<p>We recommend that you upgrade your php7.0 packages.</p>

<p>For the detailed security status of php7.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.0">https://security-tracker.debian.org/tracker/php7.0</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4353.data"
# $Id: $
