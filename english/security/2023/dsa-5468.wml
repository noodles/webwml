<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38133">CVE-2023-38133</a>

    <p>YeongHyeon Choi discovered that processing web content may
    disclose sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38572">CVE-2023-38572</a>

    <p>Narendra Bhati discovered that a website may be able to bypass the
    Same Origin Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38592">CVE-2023-38592</a>

    <p>Narendra Bhati, Valentino Dalla Valle, Pedro Bernardo, Marco
    Squarcina, and Lorenzo Veronese discovered that processing web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38594">CVE-2023-38594</a>

    <p>Yuhao Hu discovered that processing web content may lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38595">CVE-2023-38595</a>

    <p>An anonymous researcher, Jiming Wang, and Jikai Ren discovered
    that processing web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38597">CVE-2023-38597</a>

    <p>Junsung Lee discovered that processing web content may lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38599">CVE-2023-38599</a>

    <p>Hritvik Taneja, Jason Kim, Jie Jeff Xu, Stephan van Schaik, Daniel
    Genkin, and Yuval Yarom discovered that a website may be able to
    track sensitive user information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38600">CVE-2023-38600</a>

    <p>An anonymous researcher discovered that processing web content may
    lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38611">CVE-2023-38611</a>

    <p>Francisco Alonso discovered that processing web content may lead
    to arbitrary code execution.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 2.40.5-1~deb11u1.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 2.40.5-1~deb12u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5468.data"
# $Id: $
