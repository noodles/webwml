<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0108">CVE-2022-0108</a>

    <p>Luan Herrera discovered that an HTML document may be able to
    render iframes with sensitive user information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32885">CVE-2022-32885</a>

    <p>P1umer and Q1IQ discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27932">CVE-2023-27932</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may bypass Same Origin Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27954">CVE-2023-27954</a>

    <p>An anonymous researcher discovered that a website may be able to
    track sensitive user information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28205">CVE-2023-28205</a>

    <p>Clement Lecigne and Donncha O Cearbhaill discovered that
    processing maliciously crafted web content may lead to arbitrary
    code execution. Apple is aware of a report that this issue may
    have been actively exploited.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.40.1-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5396.data"
# $Id: $
