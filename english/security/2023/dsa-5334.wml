<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Martin van Kervel Smedshammer discovered that varnish, a state of the
art, high-performance web accelerator, is prone to a HTTP/2 request
forgery vulnerability.</p>

<p>See <a href="https://varnish-cache.org/security/VSV00011.html">https://varnish-cache.org/security/VSV00011.html</a> for details.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 6.5.1-1+deb11u3.</p>

<p>We recommend that you upgrade your varnish packages.</p>

<p>For the detailed security status of varnish please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/varnish">https://security-tracker.debian.org/tracker/varnish</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5334.data"
# $Id: $
