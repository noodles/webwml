<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Gregor Kopf of Secfault Security GmbH discovered that HSQLDB, a Java SQL
database engine, allowed the execution of spurious scripting commands in
.script and .log files. Hsqldb supports a <q>SCRIPT</q> keyword which is normally
used to record the commands input by the database admin to output such a
script. In combination with LibreOffice, an attacker could craft an odb
containing a "database/script" file which itself contained a SCRIPT command
where the contents of the file could be written to a new file whose location
was determined by the attacker.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 1.8.0.10+dfsg-10+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 1.8.0.10+dfsg-11+deb12u1.</p>

<p>We recommend that you upgrade your hsqldb1.8.0 packages.</p>

<p>For the detailed security status of hsqldb1.8.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/hsqldb1.8.0">\
https://security-tracker.debian.org/tracker/hsqldb1.8.0</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5436.data"
# $Id: $
