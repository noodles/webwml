<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered that an attacker-controlled
allocation using the alloca() function could result in memory
corruption, allowing to crash systemd and hence the entire operating
system.</p>

<p>Details can be found in the Qualys advisory at
<a href="https://www.qualys.com/2021/07/20/cve-2021-33910/denial-of-service-systemd.txt">https://www.qualys.com/2021/07/20/cve-2021-33910/denial-of-service-systemd.txt</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
232-25+deb9u13.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>For the detailed security status of systemd please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2715.data"
# $Id: $
