<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
                 <p><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-28660">CVE-2021-28660</a>
Debian Bug     : 983595</p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service, or information leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27170">CVE-2020-27170</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-27171">CVE-2020-27171</a></p>

    <p>Piotr Krysiuk discovered flaws in the BPF subsystem's checks for
    information leaks through speculative execution.  A local user
    could use these to obtain sensitive information from kernel
    memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3348">CVE-2021-3348</a>

    <p>ADlab of venustech discovered a race condition in the nbd block
    driver that can lead to a use-after-free.  A local user with
    access to an nbd block device could use this to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3428">CVE-2021-3428</a>

    <p>Wolfgang Frisch reported a potential integer overflow in the
    ext4 filesystem driver.  A user permitted to mount arbitrary
    filesystem images could use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a>

<p>(XSA-365)</p>

    <p>Olivier Benjamin, Norbert Manthey, Martin Mazein, and Jan
    H. Schönherr discovered that the Xen block backend driver
    (xen-blkback) did not handle grant mapping errors correctly.  A
    malicious guest could exploit this bug to cause a denial of
    service (crash), or possibly an information leak or privilege
    escalation, within the domain running the backend, which is
    typically dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a>

<p>(XSA-362), <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> (XSA-361), <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> (XSA-367)</p>

    <p>Jan Beulich discovered that the Xen support code and various Xen
    backend drivers did not handle grant mapping errors correctly.  A
    malicious guest could exploit these bugs to cause a denial of
    service (crash) within the domain running the backend, which is
    typically dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to transport handle attributes in sysfs.
    On a system acting as an iSCSI initiator, this is an information
    leak to local users and makes it easier to exploit <a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to its netlink management interface.  On
    a system acting as an iSCSI initiator, a local user could use
    these to cause a denial of service (disconnection of storage) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    correctly limit the lengths of parameters or <q>passthrough PDUs</q>
    sent through its netlink management interface.  On a system acting
    as an iSCSI initiator, a local user could use these to leak the
    contents of kernel memory, to cause a denial of service (kernel
    memory corruption or crash), and probably for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28660">CVE-2021-28660</a>

    <p>It was discovered that the rtl8188eu WiFi driver did not correctly
    limit the length of SSIDs copied into scan results.  An attacker
    within WiFi range could use this to cause a denial of service
    (crash or memory corruption) or possibly to execute code on a
    vulnerable system.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.19.181-1~deb9u1.  This update additionally fixes Debian bug
#983595, and includes many more bug fixes from stable updates
4.19.172-4.19.181 inclusive.</p>

<p>We recommend that you upgrade your linux-4.19 packages.</p>

<p>For the detailed security status of linux-4.19 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2610.data"
# $Id: $
