<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in Jackson Databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24616">CVE-2020-24616</a>

     <p>FasterXML jackson-databind 2.x before 2.9.10.6 mishandles the
     interaction between serialization gadgets and typing, related
     to br.com.anteros.dbcp.AnterosDBCPDataSource (aka Anteros-DBCP).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24750">CVE-2020-24750</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.6 mishandles the
    interaction between serialization gadgets and typing, related
    to com.pastdev.httpcomponents.configuration.JndiConfiguration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25649">CVE-2020-25649</a>

    <p>A flaw was found in FasterXML Jackson Databind, where it did not
    have entity expansion secured properly. This flaw allows
    vulnerability to XML external entity (XXE) attacks. The highest
    threat from this vulnerability is data integrity.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35490">CVE-2020-35490</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35491">CVE-2020-35491</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35728">CVE-2020-35728</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.oracle.wls.shaded.org.apache.xalan.lib.sql.JNDIConnectionPool
    (aka embedded Xalan in org.glassfish.web/javax.servlet.jsp.jstl).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36179">CVE-2020-36179</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to oadd.org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36180">CVE-2020-36180</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36181">CVE-2020-36181</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.tomcat.dbcp.dbcp.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36182">CVE-2020-36182</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.tomcat.dbcp.dbcp2.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36183">CVE-2020-36183</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related
    to org.docx4j.org.apache.xalan.lib.sql.JNDIConnectionPool.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36184">CVE-2020-36184</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp2.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36185">CVE-2020-36185</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp2.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36186">CVE-2020-36186</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36187">CVE-2020-36187</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    org.apache.tomcat.dbcp.dbcp.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36188">CVE-2020-36188</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.newrelic.agent.deps.ch.qos.logback.core.db.JNDICS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36189">CVE-2020-36189</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.8 mishandles the
    interaction between serialization gadgets and typing, related to
    com.newrelic.agent.deps.ch.qos.logback.core.db.DMCS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20190">CVE-2021-20190</a>

    <p>A flaw was found in jackson-databind before 2.9.10.7. FasterXML
    mishandles the interaction between serialization gadgets and
    typing. The highest threat from this vulnerability is to data
    confidentiality and integrity as well as system availability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.8.6-1+deb9u9.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>For the detailed security status of jackson-databind please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jackson-databind">https://security-tracker.debian.org/tracker/jackson-databind</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2638.data"
# $Id: $
