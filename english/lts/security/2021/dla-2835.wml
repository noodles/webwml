<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two heap overflows were fixed in the rsyslog logging daemon.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17041">CVE-2019-17041</a>

    <p>Heap overflow in the AIX message parser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17042">CVE-2019-17042</a>

    <p>Heap overflow in the Cisco log message parser.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.24.0-1+deb9u1.</p>

<p>We recommend that you upgrade your rsyslog packages.</p>

<p>For the detailed security status of rsyslog please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rsyslog">https://security-tracker.debian.org/tracker/rsyslog</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2835.data"
# $Id: $
