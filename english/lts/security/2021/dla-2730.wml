<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was an issue in libpam-tacplus (a
security module for using the TACACS+ authentication service) where
shared secrets such as private server keys were being added in
the clear to various logs.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.8-2+deb9u1.</p>

<p>We recommend that you upgrade your libpam-tacplus packages.</p>

<p>For the detailed security status of libpam-tacplus please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libpam-tacplus">https://security-tracker.debian.org/tracker/libpam-tacplus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2730.data"
# $Id: $
