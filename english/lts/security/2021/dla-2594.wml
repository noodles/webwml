<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Three security issues have been detected in tomcat8.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-24122">CVE-2021-24122</a>

    <p>When serving resources from a network location using the NTFS file system,
    Apache Tomcat versions 8.5.0 to 8.5.59 is susceptible to JSP source code
    disclosure in some configurations. The root cause was the unexpected
    behaviour of the JRE API File.getCanonicalPath() which in turn was caused
    by the inconsistent behaviour of the Windows API (FindFirstFileW) in some
    circumstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25122">CVE-2021-25122</a>

    <p>When responding to new h2c connection requests, Apache Tomcat could
    duplicate request headers and a limited amount of request body from one
    request to another meaning user A and user B could both see the results
    of user A's request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25329">CVE-2021-25329</a>

    <p>The fix for 2020-9484 was incomplete. When using Apache Tomcat 8.5.0 to
    8.5.61 with a configuration edge case that was highly unlikely to be used,
    the Tomcat instance was still vulnerable to <a href="https://security-tracker.debian.org/tracker/CVE-2020-9494">CVE-2020-9494</a>. Note that both
    the previously published prerequisites for <a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a> and the
    previously published mitigations for <a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a> also apply to this
    issue.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.5.54-0+deb9u6.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2594.data"
# $Id: $
