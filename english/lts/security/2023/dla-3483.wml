<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Nullsoft Scriptable Install System (NSIS)
before version 3.09 mishandles access control for the uninstaller
directory.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.04-1+deb9u1.</p>

<p>We recommend that you upgrade your nsis packages.</p>

<p>For the detailed security status of nsis please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nsis">https://security-tracker.debian.org/tracker/nsis</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3483.data"
# $Id: $
