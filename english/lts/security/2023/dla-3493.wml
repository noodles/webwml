<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in symfony, a PHP framework
for web and console applications and a set of reusable PHP components,
which could lead to information disclosure or impersonation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21424">CVE-2021-21424</a>

    <p>James Isaac, Mathias Brodala and Laurent Minguet discovered that it
    was possible to enumerate users without relevant permissions due to
    different exception messages depending on whether the user existed
    or not.  It was also possible to enumerate users by using a timing
    attack, by comparing time elapsed when authenticating an existing
    user and authenticating a non-existing user.</p>

    <p>403s are now returned whether the user exists or not if a user
    cannot switch to a user or if the user does not exist.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24894">CVE-2022-24894</a>

    <p>Soner Sayakci discovered that when the Symfony HTTP cache system is
    enabled, the response header might be stored with a <code>Set-Cookie</code>
    header and returned to some other clients, thereby allowing an
    attacker to retrieve the victim's session.</p>

    <p>The <code>HttpStore</code> constructor now takes a parameter
    containing a list of private headers that are removed from the HTTP
    response headers.  The default value for this parameter is
    <code>Set-Cookie</code>, but it can be overridden or extended by the
    application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24895">CVE-2022-24895</a>

    <p>Marco Squarcina discovered that CSRF tokens weren't cleared upon
    login, which could enable same-site attackers to bypass the CSRF
    protection mechanism by performing an attack similar to a
    session-fixation.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.4.22+dfsg-2+deb10u2.</p>

<p>We recommend that you upgrade your symfony packages.</p>

<p>For the detailed security status of symfony please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/symfony">https://security-tracker.debian.org/tracker/symfony</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3493.data"
# $Id: $
