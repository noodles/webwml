<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>LibreOffice an office productivity suite was affected by multiple
vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3874">CVE-2022-38745</a>

    <p>Libreoffice may be configured to add an empty
    entry to the Java class path.
    This may lead to run arbitrary Java code from the
    current directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0950">CVE-2023-0950</a>

    <p>Improper Validation of Array Index vulnerability in the
    spreadsheet component allows an attacker to craft a
    spreadsheet document that will cause an array index
    underflow when loaded. In the affected versions of LibreOffice
    certain malformed spreadsheet formulas, such as AGGREGATE,
    could be created with less parameters passed to the formula
    interpreter than it expected, leading to an array index
    underflow, in which case there is a risk that arbitrary
    code could be executed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2255">CVE-2023-2255</a>

    <p>Improper access control in editor components of
    LibreOffice allowed an attacker to craft
    a document that would cause external links to be loaded without prompt.
    In the affected versions of LibreOffice documents
    that used <q>floating frames</q>
    linked to external files, would load the contents of those frames
    without prompting the user for permission to do so.
    This was inconsistent with the treatment of other linked
    content in LibreOffice.</p>


<p>For Debian 10 buster, these problems have been fixed in version
1:6.1.5-3+deb10u10.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>For the detailed security status of libreoffice please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libreoffice">https://security-tracker.debian.org/tracker/libreoffice</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3526.data"
# $Id: $
