<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that when using the get_post_logout_redirect and
get_post_login_redirect functions in flask-security, an implementation of
simple security for Flask apps, it is possible to bypass URL validation and
redirect a user to an arbitrary URL by providing multiple back slashes such as
\\\evil.com/path.</p>

<p>This vulnerability is exploitable only if an alternative WSGI server other
than Werkzeug is used, or the default behaviour of Werkzeug is modified using
'autocorrect_location_headerúlse.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.7.5-2+deb10u1.</p>

<p>We recommend that you upgrade your flask-security packages.</p>

<p>For the detailed security status of flask-security please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flask-security">https://security-tracker.debian.org/tracker/flask-security</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3545.data"
# $Id: $
