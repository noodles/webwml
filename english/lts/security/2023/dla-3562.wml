<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was identified in Orthanc, a DICOM server used for
medical imaging, whereby authenticated API users had the capability to overwrite
arbitrary files and, in certain configurations, execute unauthorized code.</p>

<p>This update addresses the issue by backporting a safeguard mechanism: the
RestApiWriteToFileSystemEnabled option is now included, and it is set to <q>true</q>
by default in the /etc/orthanc/orthanc.json configuration file. Should users
wish to revert to the previous behavior, they can manually set this option
to <q>true</q> themselves.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.5.6+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your orthanc packages.</p>

<p>For the detailed security status of orthanc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/orthanc">https://security-tracker.debian.org/tracker/orthanc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3562.data"
# $Id: $
