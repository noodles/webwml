<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the fig2dev utilities for converting XFig figure files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21529">CVE-2020-21529</a>

    <p>Stack buffer overflow in bezier_spline().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21531">CVE-2020-21531</a>

    <p>Global buffer overflow in conv_pattern_index().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21532">CVE-2020-21532</a>

    <p>Global buffer overflow in setfigfont().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21676">CVE-2020-21676</a>

    <p>Stack-based buffer overflow in genpstrx_text().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32280">CVE-2021-32280</a>

    <p>NULL pointer dereference in compute_closed_spline().</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.2.7a-5+deb10u5.</p>

<p>We recommend that you upgrade your fig2dev packages.</p>

<p>For the detailed security status of fig2dev please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/fig2dev">https://security-tracker.debian.org/tracker/fig2dev</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3304.data"
# $Id: $
