<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes a NULL pointer derference and two denial of service
conditions in in protobuf.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22569">CVE-2021-22569</a>

    <p>An issue in protobuf-java allowed the interleaving of
    <code>com.google.protobuf.UnknownFieldSet</code> fields in such a way that would be
    processed out of order. A small malicious payload can occupy the parser for
    several minutes by creating large numbers of short-lived objects that cause
    frequent, repeated pauses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22570">CVE-2021-22570</a>

    <p>Nullptr dereference when a null char is present in a proto symbol. The
    symbol is parsed incorrectly, leading to an unchecked call into the proto
    file's name during generation of the resulting error message. Since the
    symbol is incorrectly parsed, the file is nullptr.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1941">CVE-2022-1941</a>

    <p>A parsing vulnerability for the <code>MessageSet</code> type in the ProtocolBuffers can
    lead to out of memory failures. A specially crafted message with multiple
    key-value per elements creates parsing issues, and can lead to a Denial of
    Service against services receiving unsanitized input.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.6.1.3-2+deb10u1.</p>

<p>We recommend that you upgrade your protobuf packages.</p>

<p>For the detailed security status of protobuf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/protobuf">https://security-tracker.debian.org/tracker/protobuf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3393.data"
# $Id: $
