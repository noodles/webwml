<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A memory allocation issue was found in iperf3, the Internet Protocol
bandwidth measuring tool, that may cause a denial of service when
encountering a certain invalid length value in TCP packets.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.6-2+deb10u1.</p>

<p>We recommend that you upgrade your iperf3 packages.</p>

<p>For the detailed security status of iperf3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/iperf3">https://security-tracker.debian.org/tracker/iperf3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3506.data"
# $Id: $
