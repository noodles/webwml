<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A Client Authentication Bypass vulnerability has been discovered in the
concurrent, real-time, distributed functional language Erlang. Impacted are
those who are running an ssl/tls/dtls server using the ssl application
either directly or indirectly via other applications. Note that the
vulnerability only affects servers that request client certification, that
is sets the option {verify, verify_peer}.</p>

<p>Additionally the source package elixir-lang has been rebuilt against the new
erlang version. The rabbitmq-server package was upgraded to version 3.8.2 to
fix an incompatibility with Erlang 22.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:22.2.7+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your erlang packages.</p>

<p>For the detailed security status of erlang please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/erlang">https://security-tracker.debian.org/tracker/erlang</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3491.data"
# $Id: $
