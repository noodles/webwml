<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in node-types-url-parse, a Node.js
module used to parse URLs, which may result in authorization bypass or
redirection to untrusted sites.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3664">CVE-2021-3664</a>

    <p>url-parse mishandles certain uses of a single (back)slash such as
    <code>https:&bsol;</code> and <code>https:/</code>, and interprets
    the URI as a relative path.  Browsers accept a single backslash
    after the protocol, and treat it as a normal slash, while url-parse
    sees it as a relative path.  Depending on library usage, this may
    result in allow/block list bypasses, SSRF attacks, open redirects,
    or other undesired behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27515">CVE-2021-27515</a>

    <p>Using backslash in the protocol is valid in the browser, while
    url-parse thinks it's a relative path.  An application that
    validates a URL using url-parse might pass a malicious link.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0512">CVE-2022-0512</a>

    <p>Incorrect handling of username and password can lead to failure to
    properly identify the hostname, which in turn could result in
    authorization bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0639">CVE-2022-0639</a>

    <p>Incorrect conversion of <code>@</code> characters in protocol in
    the <code>href</code> field can lead to lead to failure to properly
    identify the hostname, which in turn could result in authorization
    bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0686">CVE-2022-0686</a>

    <p>Rohan Sharma reported that url-parse is unable to find the correct
    hostname when no port number is provided in the URL, such as in
    <code>http://example.com:</code>.  This could in turn result in SSRF
    attacks, open redirects or any other vulnerability which depends on
    the <code>hostname</code> field of the parsed URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0691">CVE-2022-0691</a>

    <p>url-parse is unable to find the correct hostname when the URL
    contains a backspace <code>&bsol;b</code> character.  This tricks
    the parser into interpreting the URL as a relative path, bypassing
    all hostname checks.  It can also lead to false positive in
    <code>extractProtocol()</code>.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.2.0-2+deb10u2.</p>

<p>We recommend that you upgrade your node-url-parse packages.</p>

<p>For the detailed security status of node-url-parse please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-url-parse">https://security-tracker.debian.org/tracker/node-url-parse</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3336.data"
# $Id: $
