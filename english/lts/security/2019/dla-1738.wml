<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in gpsd, the Global Positioning
System daemon. A stack-based buffer overflow may allow remote attackers
to execute arbitrary code via traffic on port 2947/TCP or crafted JSON
inputs.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.11-3+deb8u1.</p>

<p>We recommend that you upgrade your gpsd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1738.data"
# $Id: $
