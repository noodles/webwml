<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3672">CVE-2014-3672</a> (XSA-180)

    <p>Andrew Sorensen discovered that a HVM domain can exhaust the hosts
    disk space by filling up the log file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3158">CVE-2016-3158</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-3159">CVE-2016-3159</a> (XSA-172)

    <p>Jan Beulich from SUSE discovered that Xen does not properly handle
    writes to the hardware FSW.ES bit when running on AMD64 processors.
    A malicious domain can take advantage of this flaw to obtain address
    space usage and timing information, about another domain, at a
    fairly low rate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3710">CVE-2016-3710</a> (XSA-179)

    <p>Wei Xiao and Qinghao Tang of 360.cn Inc discovered an out-of-bounds
    read and write flaw in the QEMU VGA module. A privileged guest user
    could use this flaw to execute arbitrary code on the host with the
    privileges of the hosting QEMU process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3712">CVE-2016-3712</a> (XSA-179)

    <p>Zuozhi Fzz of Alibaba Inc discovered potential integer overflow
    or out-of-bounds read access issues in the QEMU VGA module. A
    privileged guest user could use this flaw to mount a denial of
    service (QEMU process crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3960">CVE-2016-3960</a> (XSA-173)

    <p>Ling Liu and Yihan Lian of the Cloud Security Team, Qihoo 360
    discovered an integer overflow in the x86 shadow pagetable code. A
    HVM guest using shadow pagetables can cause the host to crash. A PV
    guest using shadow pagetables (i.e. being migrated) with PV
    superpages enabled (which is not the default) can crash the host, or
    corrupt hypervisor memory, potentially leading to privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4480">CVE-2016-4480</a> (XSA-176)

    <p>Jan Beulich discovered that incorrect page table handling could
    result in privilege escalation inside a Xen guest instance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6258">CVE-2016-6258</a> (XSA-182)

    <p>Jérémie Boutoille discovered that incorrect pagetable handling in
    PV instances could result in guest to host privilege escalation.</p></li>

<li>Additionally this Xen Security Advisory without a CVE was fixed: XSA-166

    <p>Konrad Rzeszutek Wilk and Jan Beulich discovered that ioreq handling
    is possibly susceptible to a multiple read issue.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-1.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-571.data"
# $Id: $
