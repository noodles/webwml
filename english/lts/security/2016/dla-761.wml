<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that bottle, a WSGI-framework for the Python
programming language, did not properly filter "\r\n" sequences when
handling redirections. This allowed an attacker to perform CRLF
attacks such as HTTP header injection.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.11-1+deb7u2.</p>

<p>We recommend that you upgrade your python-bottle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-761.data"
# $Id: $
