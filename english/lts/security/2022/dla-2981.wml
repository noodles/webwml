<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in lrzip, a
compression program. Invalid pointers, use-after-free and infinite
loops would allow attackers to cause a denial of service or possibly
other unspecified impact via a crafted compressed file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5786">CVE-2018-5786</a>

    <p>There is an infinite loop and application hang in the get_fileinfo
    function (lrzip.c). Remote attackers could leverage this
    vulnerability to cause a denial of service via a crafted lrz file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25467">CVE-2020-25467</a>

    <p>A null pointer dereference was discovered lzo_decompress_buf in
    stream.c which allows an attacker to cause a denial of service
    (DOS) via a crafted compressed file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27345">CVE-2021-27345</a>

    <p>A null pointer dereference was discovered in ucompthread in
    stream.c which allows attackers to cause a denial of service (DOS)
    via a crafted compressed file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27347">CVE-2021-27347</a>

    <p>Use after free in lzma_decompress_buf function in stream.c in
    allows attackers to cause Denial of Service (DoS) via a crafted
    compressed file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26291">CVE-2022-26291</a>

    <p>lrzip was discovered to contain a multiple concurrency
    use-after-free between the functions zpaq_decompress_buf() and
    clear_rulist(). This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted lrz file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.631-1+deb9u2.</p>

<p>We recommend that you upgrade your lrzip packages.</p>

<p>For the detailed security status of lrzip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lrzip">https://security-tracker.debian.org/tracker/lrzip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2981.data"
# $Id: $
