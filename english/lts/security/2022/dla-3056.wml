<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in XFCE where allowed attackers
could execute arbitrary code because xdg-open could execute a .desktop file on
an attacker-controlled FTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32278">CVE-2022-32278</a>

    <p>XFCE 4.16 allows attackers to execute arbitrary code because xdg-open
    can execute a .desktop file on an attacker-controlled FTP server.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
0.10.7-1+deb9u1.</p>

<p>We recommend that you upgrade your exo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3056.data"
# $Id: $
