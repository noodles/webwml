<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service attack in
ruby-nokogiri, a HTML, XML, SAX etc. parser written in/for the Ruby programming
language. This was caused by the use of inefficient regular expressions that
were susceptible to excessive backtracking.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24836">CVE-2022-24836</a>

	<p>CVE-2022-24836: Nokogiri is an open source XML and HTML library for
	Ruby. Nokogiri `&lt; v1.13.4` contains an inefficient regular
	expression that is susceptible to excessive backtracking when
	attempting to detect encoding in HTML documents. Users are advised to
	upgrade to Nokogiri `&gt;= 1.13.4`. There are no known workarounds for
	this issue.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.6.8.1-1+deb9u2.</p>

<p>We recommend that you upgrade your ruby-nokogiri packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3003.data"
# $Id: $
