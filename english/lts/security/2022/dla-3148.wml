<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in mediawiki, a website engine
for collaborative work.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41765">CVE-2022-41765</a>

    <p>The HTMLUserTextField exposes existence of hidden users</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41767">CVE-2022-41767</a>

    <p>The reassignEdits maintenance script does not update results in an IP
    range check on Special:Contributions</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:1.31.16-1+deb10u4.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3148.data"
# $Id: $
