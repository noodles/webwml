<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue was discovered in pcs, a corosync and pacemaker
configuration tool:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1049">CVE-2022-1049</a>

    <p>It was discovered that expired accounts were still able to login via PAM.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
0.10.1-2+deb10u1.</p>

<p>We recommend that you upgrade your pcs packages.</p>

<p>For the detailed security status of pcs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pcs">https://security-tracker.debian.org/tracker/pcs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3108.data"
# $Id: $
