<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple out-of-bounds error were discovered in qt4-x11.
The highest threat from <a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a> (at least) is to data
confidentiality the application availability.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
4:4.8.7+dfsg-11+deb9u3.</p>

<p>We recommend that you upgrade your qt4-x11 packages.</p>

<p>For the detailed security status of qt4-x11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qt4-x11">https://security-tracker.debian.org/tracker/qt4-x11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2895.data"
# $Id: $
