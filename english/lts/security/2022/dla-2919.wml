<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two issues have been discovered in python2.7:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

    <p>Python has a buffer overflow in PyCArg_repr in _ctypes/callproc.c, which may
    lead to remote code execution in certain Python applications that accept
    floating-point numbers as untrusted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

    <p>A flaw was found in Python, specifically in the FTP (File Transfer Protocol)
    client library when using it in PASV (passive) mode. The flaw lies in how
    the FTP client trusts the host from PASV response by default. An attacker
    could use this flaw to setup a malicious FTP server that can trick FTP
    clients into connecting back to a given IP address and port. This could lead
    to FTP client scanning ports which otherwise would not have been possible.
    .
    Instead of using the returned address, ftplib now uses the IP address we're
    already connected to. For the rare user who wants an old behavior, set a
    `trust_server_pasv_ipv4_address` attribute on your `ftplib.FTP` instance to
    True.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.7.13-2+deb9u6.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>For the detailed security status of python2.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2919.data"
# $Id: $
