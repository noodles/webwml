<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in lua5.3, a simple, extensible,
embeddable programming language whereby a negation overflow and
segmentation fault could be triggered in getlocal and setlocal, as
demonstrated by getlocal(3,2^31).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.3.3-1+deb9u1.</p>

<p>We recommend that you upgrade your lua5.3 packages.</p>

<p>For the detailed security status of lua5.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lua5.3">https://security-tracker.debian.org/tracker/lua5.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2381.data"
# $Id: $
