<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tobias Stoeckmann found an integer overflow issue in JSON-C, a C
library to manipulate JSON objects, when reading maliciously crafted
large files. The issue could be exploited to cause denial of service
or possibly execute arbitrary code.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.12.1-1.1+deb9u1.</p>

<p>We recommend that you upgrade your json-c packages.</p>

<p>For the detailed security status of json-c please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/json-c">https://security-tracker.debian.org/tracker/json-c</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2301.data"
# $Id: $
