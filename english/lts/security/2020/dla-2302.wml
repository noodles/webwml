<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1152">CVE-2018-1152</a>

    <p>Denial of service vulnerability caused by a
     divide by zero when processing a crafted BMP image in TJBench.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14498">CVE-2018-14498</a>

    <p>Denial of service (heap-based buffer over-read and application
    crash) via a crafted 8-bit BMP in which one or more of the color
    indices is out of range for the number of palette entries.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13790">CVE-2020-13790</a>

    <p>Heap-based buffer over-read via a malformed PPM input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14152">CVE-2020-14152</a>

    <p>jpeg_mem_available() did not honor the max_memory_to_use setting,
    possibly causing excessive memory consumption.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:1.5.1-2+deb9u1.</p>

<p>We recommend that you upgrade your libjpeg-turbo packages.</p>

<p>For the detailed security status of libjpeg-turbo please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libjpeg-turbo">https://security-tracker.debian.org/tracker/libjpeg-turbo</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2302.data"
# $Id: $
