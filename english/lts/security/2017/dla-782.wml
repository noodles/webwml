<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Icedove, Debian's version of
the Mozilla Thunderbird mail client: Multiple vulnerabilities may lead
to the execution of arbitrary code, data leakage or bypass of the content
security policy.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
45.6.0-2.</p>

<p>We recommend that you upgrade your icedove packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-782.data"
# $Id: $
