#use wml::debian::template title="Важные события, связанные с Debian" BARETITLE=true
#use wml::debian::translation-check translation="7ff6f5b6db32d0aa1bc33e38c8014c6a83685f42" maintainer="Lev Lamberov"

<p>Основным мероприятием Debian является ежегодная конференция Debian, которая называется <a
href="https://www.debconf.org/">DebConf</a>. Также проводятся несколько <a
href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>, которые представляют
собой локальные встречи, организуемые участниками Проекта Debian.
</p>

<p>Дочерние страницы <a href="https://wiki.debian.org/DebianEvents">wiki-страницы
событий Debian</a> содержат список событий (мероприятий), в которых Debian
участвовал/участвует/будет участвовать.</p>

<p>Если вы хотите помочь представлять Debian на каком-либо из приведённых на этих
страницах мероприятии, пожалуйста, напишите об этом координатору участия Debian
в этом мероприятии, указанному на соответствующей странице.</p>

<p>Если вы хотите пригласить Проект Debian участвовать в новом мероприятии,
пожалуйста, отправьте письмо об этом событии в <a href="eventsmailinglists">список рассылки,
соответствующий географическому положению события</a>.</p>

<p>Если вы хотите помочь с координированием присутствия Debian на каком-то событии,
проверьте, имеется ли соответствующая <a href="https://wiki.debian.org/DebianEvents">страница события в Debian Wiki</a>. Если
она уже есть, то на ней вы найдёте информацию об участии, либо участвуйте в развитии этой страницы. Если такой страницы нет, то вам
следует руководствоваться <a href="checklist">контрольным списком для выставочного стенда</a>.</p>

<p>Если вы хотите узнать, какие товары с логотипом Debian производятся другими
компаниями, которые также могут использоваться и в рамках ваших мероприятий, то вам
следует обратиться к нашей <a href="merchandise">странице представительской продукции</a>.</p>

<p>Если вы планируете выступить с докладом, то вам следует создать соответствующую запись на <a
href="https://wiki.debian.org/DebianEvents">странице событий в Debian Wiki</a>.
Кроме того, вы можете разместить свою презентацию в <a
href="https://wiki.debian.org/Presentations">коллекции
презентаций о Debian</a>.</p>


<h3>Различные страницы помощи</h3>

<ul>
  <li> <a href="checklist">О чём не следует забывать</a> при организации стенда</li>
  <li> <a href="https://wiki.debian.org/DebianEvents">вики-страница событий
   Debian</a></li>
  <li> <a href="material">Представительская продукция</a></li>
  <li> <a href="keysigning">Обмен подписями ключей</a></li>
  <li> <a href="https://wiki.debian.org/Presentations">Презентации о
   Debian</a></li>
  <li> <a href="admin">Задачи</a> для events@debian.org</li>
  <li> <a href="booth">Организация стенда</a> (страница практически устарела)</li>
  <li> <a href="requirements">Требования к стенду</a> (устарела)</li>
</ul>
