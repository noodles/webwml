#use wml::debian::template title="الحصول على دبيان"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25"

<p>توزَّع دبيان <a href="../intro/free">بحرّية</a>
على الإنترنت. يمكنك تنزيل جميع أشكالها من
<a href="ftplist">مرايانا</a>.
يحتوي <a href="../releases/stable/installmanual">دليل التثبيت</a>
على تفاصيل تعليمات التثبيت، أما ملاحظات الإصدارة فيمكن مراجعتها <a href="../releases/stable/releasenotes">هنا</a>.
</p>

<p>في هذه الصفحة خيارات تثبيت الإصدارة المستقرة لتوزيعة دبيان. إن كنت مهتما بالإصدارة الاختبارية أو غير المستقرة راجع <a href="../releases/">صفحة الإصدارات</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">تنزيل صورة لوسيط التثبيت</a></h2>
    <p>يمكنك تنزيل إحدى الصور التالية حسب سرعة الإنترنت المتوفرة لديك:</p>
    <ul>
      <li>صورة تثبيت <a href="netinst"><strong>صغيرة الحجم</strong></a>:
	    يمكن تنزيلها بسرعة وتسجيلها على الوسيط.
	    استخدامها يتطلب توفر اتصال إنترنت على الحاسوب الذي ستثبِّت دبيان عليه.
	<ul class="quicklist downlist">
	  <li><a title="تنزيل المثبّت لحواسيب 64 بِت (Intel و AMD)"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">صورة ISO
	      للتثبيت الشبَكي لحواسيب 64 بِت</a></li>
	  <li><a title="تنزيل المثبّت لحواسيب 32 بِت (Intel و AMD)"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">صورة ISO
	      للتثبيت الشبَكي لحواسيب 32 بِت</a></li>
	</ul>
      </li>
      <li><a href="../CD/"><strong>صورة تثبيت كاملة</strong></a> أكبر حجما: تحتوي على حزم أكثر، تسهّل التثبيت
	على الحواسيب التي ﻻ تتوفر على اتصال إنترنت.
	<ul class="quicklist downlist">
	  <li><a title="تنزيل تورنت لأقراص DVD لحواسيب 64 بِت (Intel و AMD)"
	         href="<stable-images-url/>/amd64/bt-dvd/">تورنت لحواسيب 64 بِت (DVD)</a></li>
	  <li><a title="تنزيل تورنت لأقراص DVD لحواسيب 32 بِت (Intel و AMD)"
		 href="<stable-images-url/>/i386/bt-dvd/">تورنت لحواسيب 32 بِت (DVD)</a></li>
	  <li><a title="تنزيل تورنت لأقراص CD لحواسيب 64 بِت (Intel و AMD)"
	         href="<stable-images-url/>/amd64/bt-cd/">تورنت لحواسيب 64 بِت (CD)</a></li>
	  <li><a title="تنزيل تورنت لأقراص CD لحواسيب 32 بِت (Intel و AMD)"
		 href="<stable-images-url/>/i386/bt-cd/">تورنت لحواسيب 32 بِت (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">استخدم صورة دبيان سحابية</a></h2>
    <ul>
      <li>صورة رسمية خاصة <a href="https://cloud.debian.org/images/cloud/"><strong>بالحوسبة السحابية</strong></a>:
            مبنية من طرف فريق دبيان للحوسبة السحابية،
            يمكن استخدامها مباشرة على مزوّدك بالخدمة السحابية.
        <ul class="quicklist downlist">
          <li><a title="صورة Qcow2 OpenStack لـ (Intel و AMD) 64 بِت" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">OpenStack لـ (AMD/Intel) 64 بِت (Qcow2)</a></li>
          <li><a title="صورة Qcow2 OpenStack لـ (ARM) 64 بِت" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">OpenStack لـ (ARM) 64 بِت (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">جرّب دبيان قبل تثبيتها</a></h2>
    <p>
      يمكنك تجربة دبيان بإقلاع نظام حيّ (Live) من خلال قرص CD أو قرص DVD أو مفتاح USB دون تثبيت أي ملف
      على الحاسوب. عند انتهائك من التجربة يمكنك استخدام المثبّت المضّمن
      (<a href="https://calamares.io">مثبّت Calamares</a>
      سهل الاستخدام هو المعتمد بدءا من دبيان 10 باستر).
      إن توافقت الصورة مع احتياجاتك في ما يخص الحجم و اللغة و مجموعة الحزم فهذه الطريقة
      ممكن أن تكون مناسبة لك.
      راجع <a href="../CD/live#choose_live">المعلومات حول هذه الطريقة</a>
      لتساعدك على اتخاد القرار.
    </p>
    <ul class="quicklist downlist">
      <li><a title="تنزيل تورنت النظام الحيّ لحواسيب 64 بِت (Intel و AMD)"
	     href="<live-images-url/>/amd64/bt-hybrid/">تورنت النظام الحيّ لحواسيب 64 بِت</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">شراء مجموعة من أقراص CD أو أقراص DVD
      من عند أحد باعة أقراص دبيان</a></h2>

   <p>
      العديد من الموزعين يبيعون التوزيعة بأقل من 5 دولارات أمريكية بالإضافة لتكاليف الشحن
      (راجع مواقعهم للتأكد من توفر الشحن الدولي).
      <br />
      بعض <a href="../doc/books">الكتب حول دبيان</a> تأتي معها أقراص أيضا.
   </p>

   <p>هذه بعض الميزات الجوهرية للأقراص:</p>

   <ul>
     <li>التثبيت من القرص أكثر وضوحا.</li>
     <li>يمكن تثبيت دبيان على حاسوب ﻻ يتوفر على اتصال إنترنت.</li>
	 <li>يمكن تثبيت دبيان على العديد من الحواسيب دون حصر ودون إعادة تنزيل الحزم.</li>
     <li>يمكن للقرص أن يُستخدم بسهولة لإصلاح نظام دبيان معطّل.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">شراء حاسوب مثبّت عليه دبيان</a></h2>
   <p>هناك العديد من الميزات لهذا الاختيار:</p>
   <ul>
    <li>ليس عليك تثبيت دبيان.</li>
    <li>التثبيت مضبوط مسبقا ليلاءم العتاد.</li>
    <li>يمكن للبائع أن يوفر الدعم التقني.</li>
   </ul>
  </div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
إن كان في نظامك عتاد <strong>يتطلب تحميل فيرموير غير حر</strong> مع تعريف الجهاز، يمكنك استخدام أحد
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
أرشيفات الحزم الخاصة بالفيرمويرات الشائعة</a> أو تنزيل صورة <strong>غير رسمية</strong>
تتضمن هذه الفيرمويرات <strong>غير الحرة</strong>.
تعليمات كيفية استخدام هذه الأرشيفات بالإضافة
لمعلومات حول كيفية تحميل الفيرموير خلال التثبيت متوفرة في
<a href="../releases/stable/amd64/ch06s04">دليل التثبيت</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">صور تثبيت غير رسمية للتوزيعة <q>المستقرة</q> تتضمن الفيرمويرات</a>
</p>
</div>
