#use wml::debian::translation-check translation="28bc87857803972597b697f1aafdfc05773ea8db"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el servidor de correo electrónico Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

    <p>Con la hibernación imap activa, un atacante con credenciales válidas
    para acceder al servidor de correo puede hacer que Dovecot le descubra estructuras de
    directorios en el sistema de archivos y acceder a los correos electrónicos de otros usuarios por medio de órdenes
    preparadas de una manera determinada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

    <p>Innokentii Sennovskiy informó de que la entrega de correo y el análisis sintáctico en
    Dovecot pueden provocar una caída cuando la diezmilésima parte MIME es message/rfc822 (o
    su madre era multipart/digest). Este defecto se introdujo con
    los cambios hechos anteriormente para abordar
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">\
    CVE-2020-12100</a>.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:2.3.4.1-5+deb10u5.</p>

<p>Le recomendamos que actualice los paquetes de dovecot.</p>

<p>Para información detallada sobre el estado de seguridad de dovecot, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4825.data"
