#use wml::debian::translation-check translation="47c9da48bb2e6ee7f5d51bb1bfb0e169808fcbf0"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las siguientes vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9802">CVE-2020-9802</a>

    <p>Samuel Gross descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9803">CVE-2020-9803</a>

    <p>Wen Xu descubrió que el procesado de contenido web preparado maliciosamente
    podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9805">CVE-2020-9805</a>

    <p>Un investigador anónimo descubrió que el procesado de contenido web
    preparado maliciosamente podía dar lugar a ejecución de scripts entre sitios universal («universal cross site scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9806">CVE-2020-9806</a>

    <p>Wen Xu descubrió que el procesado de contenido web preparado maliciosamente
    podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9807">CVE-2020-9807</a>

    <p>Wen Xu descubrió que el procesado de contenido web preparado maliciosamente
    podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9843">CVE-2020-9843</a>

    <p>Ryan Pickren descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a un ataque de ejecución de scripts entre sitios («cross site scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9850">CVE-2020-9850</a>

    <p>@jinmo123, @setuid0x0_ y @insu_yun_en descubrieron que un atacante
    remoto podía provocar ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13753">CVE-2020-13753</a>

    <p>Milan Crha descubrió que un atacante podía ejecutar
    órdenes fuera del entorno aislado («sandbox») bubblewrap.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.28.3-2~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4724.data"
