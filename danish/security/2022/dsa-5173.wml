#use wml::debian::translation-check translation="56cd72821e38080a5918e1d806721388e90fd0f6" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i  Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationlækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

    <p>Eric Biederman rapporterede at ukorrekt rettighedskontrol i 
    implementeringen af cgroup-procesmigrering, kunne gøre det muligt for en 
    lokal angriber at forøge rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

    <p>scsi_ioctl() var sårbar over for en informationslækage, som kun kunne 
    udnyttes af brugere med kapabiliteterne CAP_SYS_ADMIN eller 
    CAP_SYS_RAWIO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0812">CVE-2022-0812</a>

    <p>Man opdagede at RDMA-transporten til NFS (xprtrdma) fejlberegnede 
    størrelsen af meddelelsesheadere, hvilket kunne føre til en lækage af 
    følsomme oplysninger mellemen NFS-servere og -klienter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

    <p>Ali Haider opdagede en potentiel informationslækage i DMA-undersystemet. 
    På systemer, hvor funktionaliteten swiotlb er nødvendig, kunne det være 
    muligt for en lokal bruger at læse følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1011">CVE-2022-1011</a>

    <p>Jann Horn opdagede en fejl i implementeringen af FUSE (Filesystem in 
    User-Space).  En lokal bruger med rettigheder til at mounte 
    FUSE-filsystemer, kunne udnytte fejlen til at forårsage en anvendelse efter 
    frigivelse og læsning af følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-32296">CVE-2022-32296</a>

    <p>Moshe Kol, Amit Klein og Yossi Gilad opdagede en svaghed i den tilfældige 
    udvælgelse af TCP-kildeporte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

    <p>David Bouman opdagede en fejl i undersystemet netfilter, hvor funktionen 
    nft_do_chain ikke initialiserede registerdata, som nf_tables-udtryk kan læse 
    fra og skrive til.  En lokal angriber kunne drage nytte af fejlen til at 
    læse følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

    <p>Hu Jiahui opdagede en kapløbstilstand i sound-undersystemet, hvilken 
    kunne medføre en anvendelse efter frigivelse.  En lokal bruger med 
    rettigheder til at tilgå en PCM-lydenhed, kunne drage nytte af fejlen til at 
    få systemet til at gå ned eller potentielt til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

    <p>Lin Ma opdagede kapløbstilstande i amatørradiodriverne 6pack og mkiss, 
    hvilke kunne føre til en anvendelse efter frigivelse.  En lokal bruger kunne 
    udnytte fejlene til at forårsage lammelsesangreb (hukommelseskorruption 
    eller nedbrud) eller muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

    <p>Duoming Zhou opdagede en kapløbstilstand i amatørradiodriveren 6pack, 
    hvilken kunne føre til en anvendelse efter frigivelse.  En lokal bruger 
    kunne udnytte fejlen til at forårsage et lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller potentielt til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

    <p>Duoming Zhou opdagede kapløbstilstande i amatørradioprotokollen AX.25, 
    hvilke kunne føre til en anvendelse efter frigivelse eller 
    nullpointer-dereference.  En lokal bruger kunne udnytte fejlen til at 
    forårsage et lammelsesangreb (hukommelseskorruption eller nedbrud) eller 
    muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

    <p>Værktøjet TCS Robot fandt en informationslækage i undersystemet PF_KEY. 
    En lokal bruger kunne modtage en netlink-meddelelse, når en IPsec-dæmon 
    registrerede sig hos kernen, og dette kunne indeholde følsomme 
    oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1419">CVE-2022-1419</a>

    <p>Minh Yuan opdagede en kapløbstilstand i den virtuelle GPU-driver vgem, 
    hvilken kunne føre til en anvendelse efter frigivelse.  En lokal bruger med 
    rettigheder til at tilgå GPU-enheden, kunne udnytte fejlen til at forårsage 
    et lammelsesangreb (nedbrud eller hukommelseskorruption) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

    <p>En NULL-pointerdereferencefejl i implementeringen af X.25-sættet af 
    standardiserede netværksprotokoller, kunne medføre lammelsesangreb.</p>

    <p>Denne driver er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1652">CVE-2022-1652</a>

    <p>Minh Yuan opdagede en kapløbstilstand i floppy-driveren, hvilken kunne 
    føre til en anvendelse efter frigivelse.  En lokal bruger med rettigheder 
    til at tilgå en diskettedrevsenhed, kunne udnytte fejlen til at forårsage 
    et lammelsesangreb (nedbrud eller hukommelseskorruption) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

    <p>Norbert Slusarek opdagede en kapløbstilstand i perf-undersystemet, 
    hvilken kunne medføre lokal rettighedsforøgelse til root.  
    Standardindstillingerne i Debian forhindrer udnyttelse, med mindre mere 
    lempelige rettighedsindstillinger er opsat i sysctl'en 
    kernel.perf_event_paranoid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1734">CVE-2022-1734</a>

    <p>Duoming Zhou opdagede kapløbstilstande i NFC-driveren nfcmrvl, hvilke 
    kunne føre til en anvendelse efter frigivelse, dobbelt frigivelse eller 
    null-pointerdereference.  En lokal bruger kunne være i stand til at udnytte 
    disse fejl til lammelsesangreb (nedbrud eller hukommelseskorruption) eller 
    muligvis til rettighedsforøgelse.</p>

    <p>Denne driver er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

    <p>Duoming Zhou opdagede at NFC-netlinkgrænsefladen var sårbar over for et 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

    <p><q>kangel</q> rapporterede om en fejl i KVM-implementeringen til 
    x86-processorer, hvilken kunne føre til en null-pointerdereference.  En 
    lokal bruger med rettigheder til at tilgå /dev/kvm, kunne udnytte fejlen 
    til at forårsage et lammelsesangreb (nedbrud).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

    <p>Forskellige efterforskerere opdagede fejl i Intel x86-processorer, samlet 
    betegnet som MMIO Stale Data-sårbarheder.  Disse svarer til de tidligere 
    offentliggjorte Microarchitectural Data Sampling-problemer (MDS), og kunne 
    udnyttes af lokale brugere til at lække følsome oplysninger.</p>

    <p>For nogle CPU'er, kræver afhjælpelserne af disse problemer opdateret 
    mikrokode.  En opdateret intel-microcode-pakke, kan senere blive stillet til 
    rådighed.  Den opdaterede CPU-mikrokode kan også blive gjort tilgængelige 
    som en del af en systemfirmware-opdatering (<q>BIOS</q>).</p>

    <p>Flere oplysninger om afhjælpelserne, finder man i 
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html">
    eller i pakken linux-doc-4.19.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23960">CVE-2022-23960</a>

    <p>Efterforskere ved VUSec opdagede at Branch History Buffer i 
    Arm-processorer, kunne udnyttes til at oprette informationssidekanaler med 
    spekulativ udførelse.  Problemet svarer til Spectre variant 2, men kræver 
    yderligere afhjælpelser i nogle processorer.</p>

    <p>Dette blev tidligere afhjulpet for 32 bit-Arm-arkitekturer (armel og 
    armhf), og er nu også afhjulpet for 64 bit-Arm (arm64).</p>

    <p>Det kunne udnyttes til at få adgang til følsomme oplysninger fra en 
    anden sikkerhedskontekst, så som brugerrum til kernen, eller fra en KVM-gæst 
    til kernen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

    <p>Bufferoverløb i coredriveren STMicroelectronics ST21NFCA, kunne medføre 
    lammelsesangreb eller rettighedsforøgelse.</p>

    <p>Denne driver er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

    <p><q>valis</q> rapporterede om et muligt bufferoverløb i IPsec 
    ESP-transformationskoden.  En lokal bruger kunne drage nytte af fejlen til 
    at forårsage et lammelsesangreb eller til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

    <p><q>Beraphin</q> opdagede at ANSI/IEEE 802.2 LLC type 2-driveren ikke på 
    korrekt vis udførte referenceoptælling på nogle fejlstier.  En lokal 
    angriber kunne drage nytte af fejlen til at forårsage 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i 8 devices' 
    USB2CAN-grænsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i Microchip CAN BUS 
    Analyzers grænsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i EMS CPC-USB/ARM7 
    CAN/USB-grænsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29581">CVE-2022-29581</a>

    <p>Kyle Zeng opdagede en referenceoptællingsfejl i netværksklassifikatoren 
    cls_u32, hvilken kunne føre til en anvendelse efter frigivelse.  En lokal 
    bruger kunne udnytte fejlen til at forårsage et lammelsesangreb (nedbrud 
    eller hukommelseskorruption) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30594">CVE-2022-30594</a>

    <p>Jann Horn opdagede en fejl i samspillet mellem undersystemerne ptrace og 
    seccomp.  En proces lagt i en sandkasse ved hjælp af seccomp(), men stadig 
    med rettigheder til at anvende ptrace(), kunne udnytte fejlen til at fjerne 
    seccomp-begrænsningerne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

    <p>Aaron Adams opdagede en anvendelse efter frigivelse i Netfilter, hvilken 
    kunne medføre lokal rettighedsforøgelse til root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33981">CVE-2022-33981</a>

    <p>Yuan Ming fra Tsinghua University rapporterede om en kapløbstilstand i 
    floppy-driveren, som involverede anvendelse af ioctl'en FDRAWCMD, hvilken 
    kunne føre til en anvendelse efter frigivelse.  En lokal bruger med adgang 
    til en diskettedrevsenhed, kunne udnytte fejlen til at forårsage et 
    lammelsesangreb (nedbrud eller hukommelseskorruption) eller muligvis til 
    rettighedsforøgelse.  Denne ioctl er nu blevet deaktiveret som 
    standard.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer
rettet i version 4.19.249-2.</p>

<p>På grund af et problem med signeringstjensten (jf. Debians fejl #1012741), 
kan modulet vport-vxlan ikke indlæses i den signerede kerne på amd64 i denne 
opdatering.</p>

<p>Denne opdatering retter også en regression i undersystemet network scheduler 
(fejl #1013299).</p>

<p>For 32 bit-Arm-arkitekturer (armel og armhf), aktiverer denne opdateringer 
optimerede implementeringer af flere kryptografiske og CRC-algoritmer.  For i 
hvert fald AES skulle dette fjerne en timing-sidekanal, der kunne føre til en 
lækage af følsomme oplysninger.</p>

<p>Denne opdatering indeholder mange flere fejlrettelser fra de stabile 
opderinger 4.19.236-4.19.249, begge inklusive, herunder af fejl #1006346.  
Random-driveren er tilbageført fra Linux 5.19, hvilket retter talrige 
ydelses- og korrekthedsproblemer.  Nogle ændringer vil være synlige:</p>

<ul>

    <li>Entropipuljestørrelsen er nu 256 bit, i stedet for 4096.  Du kan være 
    nødt til at justere opsætningen af systemovervågnings- eller 
    brugerrumsentropi-opsamlingstjenster, til at tage højde for ændringen.</li>

    <li>På systemer uden en hardware-RNG, vil kernen måske logge flere 
    anvendelser af /dev/urandom, før den er helt initialiseret.  Disse 
    anvendelser blev tidligere optalt for lavt, og er dermed ikke en 
    regression.</li>

</ul>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5173.data"
