#use wml::debian::cdimage title="Hämta Debian-cd-/-dvd-avbildningar via http/ftp" BARETITLE=true
#use wml::debian::translation-check translation="c64bdf15d82617c46942470ac0846ed7ec8b0f74"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>Hämta inte cd- eller dvd-avbildningar med din webbläsare på samma
sätt som du hämtar andra filer!</strong>
Skälet är att om hämtningen misslyckas tillåter de flesta webbläsare inte
dig att återuppta hämtningen där den misslyckades.
</p>
</div>


<p>Istället ber vi dig använda ett verktyg som stödjer återhämtning -
beskrivs typiskt som en <q>Hämtningshanterare</q>. Det finns många
insticksmoduler för webbläsare som kan sköta detta, eller så kan du
vilja titta på ett separat program. Under Linux/Unix, så kan du
använda
<a href="https://aria2.github.io/">aria2</a>,
<a href="https://sourceforge.net/projects/dfast/">wxDownload Fast</a>
eller (på kommandoraden)
<q><tt>wget -c </tt><em>URL</em></q> eller
<q><tt>curl -C - -L -O </tt><em>URL</em></q>.
Det finns många flera alternativ som listas i <a
href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">en jämförelse
över hämtningshanterare</a>.</p>


<p>Följande Debianavbildningar finns att hämta direkt:</p>

<ul>
 <li>
  <a href="#stable">Officiella cd-/dvd-avbildningar för den
  stabila utgåvan (<q lang="en">stable</q>)</a>.
 </li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Officiella
  CD/DVD-avbildningar av <q>uttestnings</q>-distributionen (<em>genereras
  varje vecka</em>)</a></li>

</ul>

<p>
Se även:
</p>

<ul>
 <li>
  En fullständig
  <a href="#mirrors">förteckning över <tt>debian-cd/</tt>-speglar</a>.
 </li>

 <li>
  För <q>nätverksinstallation</q>, se sidan om
  <a href="../netinst/">nätverksinstallationer</a>.
 </li>

 <li>
  För avbildningar av <q>uttestningsutgåvan</q> se
  <a href="$(DEVEL)/debian-installer/">Debian-Installer-sidan</a>.
 </li>
</ul>

<hr />

<h2><a name="stable">Officiella cd-avbildningar för den <q>stabila</q>
utgåvan</a></h2>

<p>
För att installera Debian på en maskin som saknar Internetförbindelse kan du
använda cd- (700 Mbyte styck) eller dvd-avbildningar (4,7 Gbyte styck).
Hämta den första cd- eller dvd-avbildningsfilen, skriv den med en
cd-/dvd-inspelare (eller ett usb-minne på i386- och amd64-anpassningarna) och 
starta om datorn från den.
</p>

<p>
Den <strong>första</strong> cd:n/dvd:n innehåller alla filer som behövs för
att installera ett vanligt Debiansystem.
<br />
För att undvika onödiga hämtningar bör du <strong>inte</strong> hämta de
andra cd-/dvd-avbildningsfilerna såvida du inte vet att du behöver paket
från dem.
</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>
Länkarna nedan pekar på avbildningsfiler som är upp till 700 Mbyte stora,
vilket gör att de passar att skriva till vanliga cd-r(w)-media:
</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>
Länkarna nedan pekar på avbildningsfiler som är upp till 4,7 Gbyte stora,
vilket gör att de passar att skriva till vanliga dvd-r/dvd+r och liknande
media:
</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Se till att läsa dokumentationen innan du installerar.
<strong>Om du bara tänker läsa ett dokument</strong> innan du installerar,
se vår
<a href="$(HOME)/releases/stable/i386/apa">installationshjälp</a>, en snabb
genomgång av installationsprocessen.
Dessutom finns följande nyttiga dokumentation:
</p>

<ul>
 <li>
  <a href="$(HOME)/releases/stable/installmanual">Installationsguide</a>,
  de detaljerade installationsinstruktionerna.
 </li>
 <li>
  <a href="https://wiki.debian.org/DebianInstaller">Dokumentation för
  Debian-installer</a>, bland annat vanliga frågor med svar.
 </li>
 <li>
  <a href="$(HOME)/releases/stable/debian-installer/#errata">Errata för
  Debian-installer</a>, en förteckning över kända problem i
  installationsprogrammet.
 </li>
</ul>

<hr />

<h2><a name="mirrors">Registrerade speglar av
<q>debian-cd</q>-arkivet</a></h2>

<p>Observera att <strong>vissa speglar inte är à jour</strong> &ndash;
innan du hämtar något, kontrollera att versionsnumret på avbildningarna är
desamma som anges <a href="../#latest">på denna webbplats</a>!
Dessutom är det möjligt att vissa speglar på grund av platsbrist inte
speglar alla avbildningar (framförallt gäller detta dvd-avbildningar).
</p>

<p>
<strong>Om du är osäker bör du använda
<a href="https://cdimage.debian.org/debian-cd/">den primära servern för
cd-avbildningar</a> i Sverige</strong> eller pröva
<a href="https://debian-cd.debian.net/">den experimentella automatiska
speglingsväljaren</a> som automatiskt kommer att omdirigera dig till en
närbelägen spegling som är känd att innehålla den senaste versionen.</p>

<p>Vill du erbjuda Debian-cd-avbildningar på din spegel?
I så fall, se
<a href="../mirroring/">instruktionerna för att skapa en
cd-avbildningsspegel</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
