#use wml::debian::translation-check translation="f50f223cba2263643e2b4d2c990eba1959671107" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenEXR, une bibliothèque
et des outils pour le format d’image de haute qualité OpenEXR. Un attaquant
pourrait causer un déni de service (DoS) à l’aide d’un plantage d'application
et une consommation excessive de mémoire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.2.0-11+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openexr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openexr, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openexr">\
https://security-tracker.debian.org/tracker/openexr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2701.data"
# $Id: $
