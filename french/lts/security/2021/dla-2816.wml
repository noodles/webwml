#use wml::debian::translation-check translation="f0e093dffb43f60249f1141b3fdddc34b4d2c79f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Icinga 2, une
application généraliste de surveillance. Un attaquant pourrait récupérer
des informations sensibles comme des mots de passe de service et des
salages de ticket en interrogeant l'API web, ou en interceptant des
connexions chiffrées insuffisamment contrôlées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32739">CVE-2021-32739</a>

<p>Il existe une vulnérabilité qui pourrait permettre une élévation de
privilèges des utilisateurs authentifiés de l'API. Avec une accréditation
en lecture seule d'utilisateurs, un attaquant peut voir la plupart des
attributs des objets de configuration y compris « ticket_salt »
d'ApiListener. Ce salage est suffisant pour calculer un ticket pour chaque
Nom Commun (CN) possible. Un ticket, le certificat maître du nœud et un
certificat auto-signé sont suffisants pour demander avec succès le
certificat désiré à Icinga. Ce certificat peut à son tour être utilisé pour
dérober l'identité d'un point de terminaison ou d'un utilisateur de l'API.
Consultez également les procédures manuelles complémentaires
<a href="https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#change-ticket-salt">\
https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#change-ticket-salt</a> et
<a href="https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#replace-icinga-ca">\
https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#replace-icinga-ca</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32743">CVE-2021-32743</a>

<p>Certaines des fonctionnalités d'Icinga 2 qui ont besoin d'accréditations
pour des services externes exposent ces accréditations à travers l'API pour
authentifier les utilisateurs de l'API avec des droits de lecture pour les
types d'objet correspondants. IdoMysqlConnection et IdoPgsqlConnection
exposent le mot de passe de l'utilisateur utilisé pour se connecter à la
base de données. Un attaquant qui obtient ces accréditations peut usurper
l'identité d'Icinga pour ces services et y ajouter, modifier et supprimer
des informations. Si des accréditations dotées de plus de permissions sont
utilisées, cela accroît l'impact en conséquence.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37698">CVE-2021-37698</a>

<p>InfluxdbWriter et Influxdb2Writer ne vérifiaient pas le certificat du
serveur bien qu'une autorité de certification ait été spécifiée. Les
instances d'Icinga 2 qui se connectent à n'importe laquelle des bases de
données de séries chronologiques (TSDB) en se servant de TLS sur une
infrastructure qui peut être usurpée, devraient immédiatement être mises à
niveau. Ces instances modifient aussi les accréditations (s'il y en a)
utilisées par la fonction d'écriture de TSDB pour s'authentifier à la
TSDB.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.6.0-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icinga2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de icinga2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/icinga2">\
https://security-tracker.debian.org/tracker/icinga2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2816.data"
# $Id: $
