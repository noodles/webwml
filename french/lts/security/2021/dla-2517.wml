#use wml::debian::translation-check translation="18fabfef6df13e6df691c79d1fbbbf72785dbc7f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux problèmes dans le serveur IMAP
Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

<p>Un problème a été découvert dans Dovecot avant la version 2.3.13 en utilisant
IMAP IDLE, un attaquant authentifié peut déclencher la sortie d’hibernation
à l'aide de paramètres contrôlés par l’attaquant, conduisant à un accès à des
messages d’autres utilisateurs (et à une divulgation de chemin).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

<p>Dovecot avant la version 2.3.13 valide incorrectement les entrées dans lda,
lmtp et imap, conduisant à un plantage d'application à l'aide d'un message de
courriel contrefait avec un certain choix pour dix mille parties MIME.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.2.27-3+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2517.data"
# $Id: $
