#use wml::debian::translation-check translation="7ffef0023f34d7c53a833c38688b33aa4f60fd6b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été détectés dans zeromq3.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20234">CVE-2021-20234</a>

<p>Fuite de mémoire dans un client provoquée par des serveurs malveillants sans
CURVE/ZAP.</p>

<p>D’après la description du problème
[<a href="https://github.com/zeromq/libzmq/security/advisories/GHSA-wfr2-29gj-5w87">1</a>],
lorsqu’un tube (pipe) traite un délimiteur et n’est pas dans un état actif mais
a encore un message non terminé, le message est divulgué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20235">CVE-2021-20235</a>

<p>Dépassement de tas lors de la réception de paquets ZMTP version 1 mal
formés.</p>

<p>D’après la description du problème
[<a href="https://github.com/zeromq/libzmq/security/advisories/GHSA-fc3w-qxf5-7hp6">2</a>],
l’allocateur statique a été implémenté pour diminuer sa taille enregistrée
de la même façon que l’allocateur partagé. Mais ce n’est pas nécessaire, et ne
devrait pas l’être, car au contraire de celui partagé, l’allocateur statique
utilise toujours un tampon statique, avec une taille définie par l’option de
socket ZMQ_IN_BATCH_SIZE (par défaut 8192), aussi modifier la taille engage la
bibliothèque dans un dépassement de tas. L’allocateur statique est utilisé
seulement avec les pairs ZMTP version 1.</p>

</li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.2.1-4+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zeromq3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zeromq3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zeromq3">\
https://security-tracker.debian.org/tracker/zeromq3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2588.data"
# $Id: $
