#use wml::debian::translation-check translation="ef131b7c538ec9dfb7045ec16129981b3c3dba82" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Trois problèmes ont été découverts dans php5, un langage de script embarqué
dans du HTML et côté serveur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7064">CVE-2020-7064</a>

<p>Une lecture hors limites d’un octet pourrait éventuellement amener une
divulgation d'informations ou un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7066">CVE-2020-7066</a>

<p>Une URL contenant le caractère zero (\0) serait tronquée à cette valeur. Cela
pourrait conduire certains logiciels à faire des hypothèses incorrectes et,
éventuellement, envoyer des informations au mauvais serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7067">CVE-2020-7067</a>

<p>L’utilisation d’une chaîne encodée sous forme d’URL mal formée pourrait
amener à une lecture hors limites.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.40+dfsg-0+deb8u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2188.data"
# $Id: $
