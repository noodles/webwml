#use wml::debian::translation-check translation="5344537c0081885c540b17b5e686895262dff852" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de squid3 publiée dans l’annonce DLA-2278-1 contenait un
correctif incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12523">CVE-2019-12523</a>
qui empêchait les services basés sur le protocole icap ou ecap de fonctionner
correctement. Des paquets squid3 mis à jour sont maintenant disponibles pour
corriger le problème.</p>

<p>De plus, le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12529">CVE-2019-12529</a>
a été amélioré pour utiliser plus de code de la bibliothèque nettle de
chiffrement de Debian.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.5.23-5+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2278-2.data"
# $Id: $
