#use wml::debian::translation-check translation="de064b712c8853033e6aa9bb95df78d928e4e52d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans Apache Tomcat 8, un
serveur en Java d’applications. Un nombre excessif de flux concurrents pouvait
aboutir à ce que des utilisateurs voient des réponses pour des ressources
inattendues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13943">CVE-2020-13943</a>

<p>Si un client HTTP/2 connecté à Apache Tomcat, versions 10.0.0-M1 à 10.0.0-M7,
9.0.0.M1 à 9.0.37 ou 8.5.0 à 8.5.57, outrepassait le nombre maximal de flux
concurrents pour une connexion (en violation du protocole HTTP/2), il était
possible qu’une requête suivante faite sur cette connexion contienne des
en-têtes HTTP — y compris des pseudo-entêtes HTTP/2 — d’une précédente requête
plutôt que les entêtes désirés. Cela pouvait conduire à ce que des utilisateurs
voient des réponses pour des ressources inattendues.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 8.5.54-0+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2407.data"
# $Id: $
