#use wml::debian::translation-check translation="2c91f9f0e63a99464dda2301a062e96365ab008a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>php-pear dans php5 contient les vulnérabilités CWE-502 (désérialisation de
données non fiables) et CWE-915 (modification improprement contrôlée d’attributs
d’objet déterminés dynamiquement) dans sa classe Archive_Tar. Lorsque extract
est appelé sans un chemin préfixé particulier, cela peut provoquer la
désérialisation en contrefaisant un fichier tar avec
« phar://[path_vers_fichier_phar_malveillant] » comme path. L’injection d’objet
peut être utilisée pour déclencher destruct dans les classes PHP chargées, avec
une exécution de code possible à distance conduisant à des suppressions de
fichiers ou éventuellement leurs modifications.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 5.6.39+dfsg-0+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1674.data"
# $Id: $
