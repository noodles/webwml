#use wml::debian::translation-check translation="f4b202ff150c549a294311c3e562eb88ac86c310" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans openldap, un
serveur et des outils pour fournir un service d’annuaire autonome.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13057">CVE-2019-13057</a>

<p>Quand l’administrateur du serveur confie les privilèges rootDN (admin de base
de données) pour certaines bases de données mais veut conserver une séparation
(par exemple, pour les déploiements « multi-tenant »), slapd ne stoppe pas
correctement un rootDN dans la requête d’autorisation sous une identité d’une
autre base de données lors d’une liaison SASL ou avec un contrôle proxyAuthz
(RFC 4370). (Ce n’est pas une configuration courante de déployer un système où
l’administrateur du serveur et l’administrateur DB agréent des niveaux de
confiance différents.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13565">CVE-2019-13565</a>

<p>Lors de l’utilisation du chiffrement d’authentification et de session SASL
et de l’appui sur les couches de sécurité dans les contrôles d’accès slapd,
il est possible d’obtenir un accès qui serait autrement refusé par une liaison
simple pour n’importe quelle identité assurée dans ces ACL. Après que la première
liaison SASL soit assurée, la valeur the sasl_ssf est retenue pour toutes les
nouvelles connexions non SASL. En fonction de la configuration des ACL, cela
peut affecter différents types d’opérations (recherches, modifications, etc.).
En d’autres mots, une étape réussie d’authentification accomplie par un
utilisateur affecte les exigences d’autorisation d’un autre utilisateur.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.40+dfsg-1+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openldap.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1891.data"
# $Id: $
