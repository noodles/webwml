#use wml::debian::translation-check translation="c4a1c2bdc93f45db86669b5a8ce1e0f17a23954d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes dans elfutils, une collection d’utilitaires pour gérer
des objets ELF, ont été découverts soit par des tests à données aléatoires ou
en utilisant un AddressSanitizer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7665">CVE-2019-7665</a>

<p>Dû un problème de dépassement de tampon basé sur le tas dans la fonction
elf32_xlatetom(), une entrée ELF contrefaite peut causer des erreurs de
segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7150">CVE-2019-7150</a>

<p>Ajout d’un test de validité pour des lectures partielles de données dynamiques
de fichier central.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7149">CVE-2019-7149</a>

 <p>Dû à un problème de dépassement de tampon basé sur le tas dans la fonction
read_srclines(), une entrée ELF contrefaite peut causer des erreurs de
segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18521">CVE-2018-18521</a>

 <p>En utilisant un fichier ELF contrefait, contenant un zéro sh_entsize, une
vulnérabilité de  division par zéro pourrait permettre à des attaquants distants
de causer un déni de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18520">CVE-2018-18520</a>

 <p>À l’aide de tests (fuzzing), un problème de déréférencement d’adresse non
valable dans la fonction elf_end a été découvert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18310">CVE-2018-18310</a>

 <p>À l’aide de tests (fuzzing), un problème de lecture d’adresse non valable
dans eu-stack a été trouvé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16062">CVE-2018-16062</a>

 <p>En utilisant un AddressSanitizer, un dépassement de tampon basé sur le tas a
 été découvert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7613">CVE-2017-7613</a>

 <p>À l’aide de tests (fuzzing), il a été découvert qu’un échec d’allocation
n’était pas géré correctement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7612">CVE-2017-7612</a>

 <p>En utilisant un fichier ELF contrefait, contenant un sh_entsize non valable,
 des attaquants distants pourraient causer un déni de service (plantage
 d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7611">CVE-2017-7611</a>

 <p>En utilisant un fichier ELF contrefait, des attaquants distants pourraient
 causer un déni de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7610">CVE-2017-7610</a>

<p>En utilisant un fichier ELF contrefait, des attaquants distants pourraient
 causer un déni de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7608">CVE-2017-7608</a>

 <p>À l’aide de tests (fuzzing), un dépassement de tampon basé sur le tas a été
 détecté.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.159-4.2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets elfutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1689.data"
# $Id: $
