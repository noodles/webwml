#use wml::debian::translation-check translation="0aedc949099da904301f38f9612da92bf35337db" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans la bibliothèque de
compression zlib.</p>

<p>Danilo Ramos a découvert qu'un traitement incorrect de la mémoire dans
la gestion de <q>deflate</q> dans zlib pouvait avoir pour conséquences un
déni de service ou éventuellement l'exécution de code arbitraire lors du
traitement d'une entrée contrefaite pour l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:1.2.8.dfsg-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zlib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zlib, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zlib">\
https://security-tracker.debian.org/tracker/zlib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2968.data"
# $Id: $
