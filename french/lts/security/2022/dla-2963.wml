#use wml::debian::translation-check translation="003b6ca206083a0a95ca94a7feed8689d06a69cb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications dans tzdata 2022a. Les
changements notables sont :</p>

<p>– règles de l'heure d'été adaptées pour la Palestine, déjà en vigueur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2021a-0+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tzdata, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tzdata">\
https://security-tracker.debian.org/tracker/tzdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2963.data"
# $Id: $
