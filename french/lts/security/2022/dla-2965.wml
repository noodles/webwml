#use wml::debian::translation-check translation="44b2d3591de4808bfa6deaaa689ebf7bd657a7b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Cacti, une interface
web pour représenter graphiquement les systèmes de surveillance, menant à
un contournement d'authentification et des scripts intersites. Un attaquant
peut obtenir l'accès à des zones non autorisées et usurper l'identité
d'autres utilisateurs, sous certaines conditions.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10060">CVE-2018-10060</a>

<p>Cacti a des scripts intersites, parce qu'il ne rejette pas correctement
des caractères non prévus, liés à l'utilisation de la fonction sanitize_uri
dans lib/fonctions.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10061">CVE-2018-10061</a>

<p>Cacti a des scripts intersites, parce qu'il procède à certains appels
de <q>htmlspecialchars</q> sans le drapeau ENT_QUOTES (ces appels se
produisent quand la fonction html_escape dans lib/html.php n'est pas
utilisée).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11025">CVE-2019-11025</a>

<p>Aucune protection ne se produit avant d’imprimer la valeur de la chaîne
community de SNMP (options de SNMP) dans le cache du scrutateur de View,
menant à un script intersite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7106">CVE-2020-7106</a>

<p>Cacti a des scripts intersites stockés dans plusieurs fichiers comme
cela est montré par le paramètre de description dans data_sources.php (une
chaîne brute de la base de données qui est affichée par $header pour
déclencher le script intersite).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13230">CVE-2020-13230</a>

<p>La désactivation d'un compte d'utilisateur n'invalide pas immédiatement
toutes les permissions accordées à ce compte (par exemple la permission de
consulter les journaux).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23226">CVE-2020-23226</a>

<p>Plusieurs vulnérabilités de script intersite (XSS) existent dans
plusieurs fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23225">CVE-2021-23225</a>

<p>Cacti permet à des utilisateurs authentifiés dotés des permissions de
gestion d'utilisateurs d'injecter un script web arbitraire ou du code HTML
dans le champ <q>new_username</q> durant la création d'un nouvel
utilisateur au moyen de la méthode <q>Copy</q> d'user_admin.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0730">CVE-2022-0730</a>

<p>Dans certaines conditions de ldap, l'authentification de Cacti peut être
contournée avec certains types d'identifiants.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.8.8h+ds1-10+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cacti.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cacti, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2965.data"
# $Id: $
