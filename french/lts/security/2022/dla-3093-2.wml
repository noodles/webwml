#use wml::debian::translation-check translation="1118b972a77a8372a655c7d21a21b1c64eaa5858" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité annoncée sous le nom de DLA 3093-1 qui
incluait un correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32224">CVE-2022-32224</a>
provoquait une régression due à une incompatibilité avec la version 2.5 de
ruby 2.5. Nous avons supprimé le correctif susmentionné. Les paquets rails
mis à jour sont maintenant disponibles.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2:5.2.2.1+dfsg-1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rails, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rails">\
https://security-tracker.debian.org/tracker/rails</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3093-2.data"
# $Id: $
