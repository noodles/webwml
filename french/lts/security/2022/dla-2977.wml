#use wml::debian::translation-check translation="909b66e78ea2905a72c5258bea192a19ce234e9d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'écriture de fichier arbitraire a été découverte dans
xz-utils, qui fournit des utilitaires de compression au format XZ.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
5.2.2-1.2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xz-utils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xz-utils, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xz-utils">\
https://security-tracker.debian.org/tracker/xz-utils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2977.data"
# $Id: $
