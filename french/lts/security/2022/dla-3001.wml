#use wml::debian::translation-check translation="31d756f74939b092dc5be2c4fec68a39c983c0b0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet com.google.code.gson:gson dans les versions antérieures
à 2.8.9 est vulnérable à une désérialisation de données non fiables au
moyen de la méthode writeReplace() dans les classes internes, qui pouvait
conduire à des attaques par déni de service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.4-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgoogle-gson-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgoogle-gson-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgoogle-gson-java">\
https://security-tracker.debian.org/tracker/libgoogle-gson-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3001.data"
# $Id: $
