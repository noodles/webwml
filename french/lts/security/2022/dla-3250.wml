#use wml::debian::translation-check translation="1efc51596f06c911dd225fa95d09a203f6943f60" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans multipath-tools, un ensemble
d’outils de programmation pour gérer des mappages de périphériques disques
multichemins, qui pouvaient être utilisés par des attaquants locaux pour obtenir
les droits du superutilisateur, créer des répertoires ou écraser des fichiers
à l'aide d'une attaque par liens symboliques.</p>

<p>Veuillez noter que le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>
implique le basculement de /dev/shm à systemd-tmpfiles (/run/multipath-tools).
En cas d’accès direct auparavant à /dev/shm, veuillez mettre à jour votre
configuration vers le nouveau chemin pour faciliter ce changement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>

<p>multipath-tools, versions 0.7.7 jusqu’à 0.9.x et avant 0.9.2 permet à des
utilisateurs locaux d’obtenir un accès superutilisateur, si exploité en
conjonction avec
 <a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>.
Des utilisateurs locaux capables d’accéder à /dev/shm pouvaient changer les
liens symboliques dans multipathd à cause d’une gestion incorrecte de liens
symboliques, ce qui pouvait conduire à des écritures contrôlées de fichier
en dehors du répertoire /dev/shm. Cela pouvait être utilisé indirectement
pour une élévation locale des privilèges à ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>

<p>multipath-tools, versions 0.7.0 jusqu’à 0.9.x avant 0.9.2 permet à des
utilisateurs locaux d’obtenir les droits du superutilisateur, si exploité seul
ou en conjonction avec le
 <a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>.
Des utilisateurs locaux capables d’écrire dans des sockets de domaine UNIX
pouvaient contourner les contrôles d’accès et manipuler la configuration de
multichemins. Cela pouvait conduire à une élévation locale des privilèges à
ceux du superutilisateur. Cela pouvait se produire parce qu’un attaquant pouvait
répéter un mot-clé, mal géré à cause de l’utilisation d’un opérateur de
manipulation de bits ADD au lieu d’un OR.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.7.9-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets multipath-tools.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de multipath-tools,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/multipath-tools">\
https://security-tracker.debian.org/tracker/multipath-tools</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3250.data"
# $Id: $
