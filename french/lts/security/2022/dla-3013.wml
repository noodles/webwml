#use wml::debian::translation-check translation="ef80df8b8ae80a7383f53bf6ca8adfa5eff87ae3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jakub Wilk a découvert une élévation de privilèges locale dans
needrestart, un utilitaire pour vérifier quels démons ont besoin d'être
redémarrés après les mises à niveau de bibliothèques. Les expressions
rationnelles pour détecter les interpréteurs Perl, Python et Ruby ne sont
pas ancrées, permettant à un utilisateur local d'élever ses privilèges
lorsque needrestart tente de détecter si les interpréteurs utilisent
d'anciens fichiers source.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.11-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets needrestart.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de needrestart,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/needrestart">\
https://security-tracker.debian.org/tracker/needrestart</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3013.data"
# $Id: $
