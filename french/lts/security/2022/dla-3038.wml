#use wml::debian::translation-check translation="11e387ea6a5a5018656b53175ee649219f536045" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>debian-security-support, le vérificateur de couverture de prise en
charge de sécurité pour Debian, a été mis à jour dans stretch-security pour
indiquer la fin de vie des paquets suivants :</p>

<p>* keystone :
consultez <a href="https://lists.debian.org/debian-lts/2020/05/msg00011.html">\
https://lists.debian.org/debian-lts/2020/05/msg00011.html</a> pour
davantage d'informations.</p>

<p>* libspring-java :
consultez <a href="https://lists.debian.org/debian-lts/2021/12/msg00008.html">\
https://lists.debian.org/debian-lts/2021/12/msg00008.html</a> pour
davantage d'informations.</p>

<p>* guacamole-client :
consultez <a href="https://lists.debian.org/debian-lts/2022/01/msg00015.html">\
https://lists.debian.org/debian-lts/2022/01/msg00015.html</a> pour
davantage d'informations.</p>

<p>* gpac :
consultez <a href="https://lists.debian.org/debian-lts/2022/04/msg00008.html">\
https://lists.debian.org/debian-lts/2022/04/msg00008.html</a> pour
davantage d'informations.</p>

<p>* ansible :
l'absence d'une suite de tests effective rend une prise en charge correcte
impossible.</p>

<p>* mysql-connector-java :
les détails sur les vulnérabilités de sécurité n'ont pas été divulgués.
MySQL a été remplacé par MariaDB. Nous recommandons d'utiliser à la place
mariadb-connector-java.</p>

<p>* ckeditor3 :
consultez <a href="https://lists.debian.org/debian-lts/2022/05/msg00060.html">\
https://lists.debian.org/debian-lts/2022/05/msg00060.html</a> pour
davantage d'informations.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:9+2022.06.02.</p>

<p>Nous vous recommandons de mettre à jour vos paquets
debian-security-support.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de
debian-security-support, veuillez consulter sa page de suivi de sécurité à
l'adresse :
<a href="https://security-tracker.debian.org/tracker/debian-security-support">\
https://security-tracker.debian.org/tracker/debian-security-support</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3038.data"
# $Id: $
