#use wml::debian::translation-check translation="307a6a4fa431913879b84ec9b2b69951d07d8d3a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une potentielle vulnérabilité de déni de service à distance
dans le résolveur DNS knot-resolver validant les DNSSEC.</p>

<p>Des attaquants pouvaient provoquer un déni de service au moyen d'une
consommation excessive du processeur en exploitant une complexité
algorithmique : pendant une attaque, un serveur faisant autorité devait
renvoyer de grands jeux de noms de serveur ou d'adresses.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40188">CVE-2022-40188</a>

<p>Les versions de Knot Resolver antérieures à 5.5.3 permettent à des
attaquants distants de provoquer un déni de service (consommation excessive
du processeur) à cause d'une complexité algorithmique. Pendant une attaque,
un serveur faisant autorité doit renvoyer de grands jeux de noms de
serveur (NS) ou d'adresses.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 3.2.1-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets knot-resolver.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3139.data"
# $Id: $
