#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans audiofile.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6829">CVE-2017-6829</a>

<p>Possibilité pour des attaquants distants de provoquer un déni de service
(plantage) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6830">CVE-2017-6830</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6834">CVE-2017-6834</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6831">CVE-2017-6831</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6832">CVE-2017-6832</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6838">CVE-2017-6838</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6839">CVE-2017-6839</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6836">CVE-2017-6836</a>

<p>Dépassement de tampon de tas permettant à des attaquants distants de
provoquer un déni de service (plantage) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6833">CVE-2017-6833</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6835">CVE-2017-6835</a>

<p>La fonction runPull permet à des attaquants distants de provoquer un déni de
service (erreur de division par zéro et plantage) à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6837">CVE-2017-6837</a>

<p>Possibilité pour des attaquants distants de provoquer un déni de service
(plantage) à l’aide de vecteurs relatifs à un grand nombre de coefficients.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.3.4-2+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets audiofile.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-867.data"
# $Id: $
