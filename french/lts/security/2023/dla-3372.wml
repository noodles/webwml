#use wml::debian::translation-check translation="76ea1ab68b11352f39fe69b9201162633c7645d4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn a découvert qu’un défaut d’utilisation de mémoire après
libération dans l’extension Composite du serveur X.org pouvait conduire à une
élévation des privilèges si le serveur X était exécuté sous le compte du
superutiliseur.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:1.20.4-1+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xorg-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xorg-server,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xorg-server">\
https://security-tracker.debian.org/tracker/xorg-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3372.data"
# $Id: $
