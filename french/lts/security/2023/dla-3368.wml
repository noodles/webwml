#use wml::debian::translation-check translation="f7745dcc93992da2202fc944d6dcdaf4ebdff433" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans LibreOffice, une suite
bureautique, conduisant à une exécution de script arbitraire, une validation
incorrecte de certificat et un faible chiffrement du stockage de mot de passe
dans la base de données de configuration d’utilisateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25636">CVE-2021-25636</a>

<p>Utilisation uniquement de X509Data — LibreOffice prend en charge la
signature numérique de documents et macros ODF dans les documents, fournissant
une aide visuelle pour certifier que le document n’a pas été modifié depuis la
dernière signature et que celle-ci est valable. Une vulnérabilité de validation
incorrecte de certificat dans LibreOffice permettait à un attaquant de créer un
document ODF signé numériquement en manipulant le flux documentsignatures.xml ou
macrosignatures.xml dans le document pour contenir <q>X509Data</q> et
<q>KeyValue</q> enfants de l’étiquette <q>KeyInfo</q>, qui lors de son ouverture
faisait que LibreOffice vérifiait en utilisant la <q>KeyValue</q>, mais en
reportant la vérification avec la valeur <q>X509Data</q> non concernée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3140">CVE-2022-3140</a>

<p>Validation insuffisante des schémas d’URI <q>vnd.libreoffice.command</q>
— LibreOffice gère les schémas d’URI d’Office pour permettre l’intégration de
navigateur de LibreOffice avec le serveur SharePoint de MS. Un schéma
supplémentaire <q>vnd.libreoffice.command</q> spécifique à LibreOffice a été
ajouté. Dans les versions affectées de LibreOffice, les liens utilisant ce
schéma pouvaient être construits pour appeler des macros internes avec des
arguments arbitraires. Ces liens, lorsque cliqués ou activés par des évènements
de document, pouvaient aboutir à une exécution de script arbitraire sans aucun
avertissement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26305">CVE-2022-26305</a>

<p>Comparaison d’auteurs avec Thumbprint — une vulnérabilité de validation
incorrecte de certificat dans LibreOffice existait lorsque la vérification de
la signature d'une macro par un auteur autorisé était réalisée en mettant
seulement en correspondance le numéro de série et une chaine émise par le
certificat utilisé avec un certificat fiable. Cela n’était pas suffisant pour
vérifier que la macro était vraiment signée avec le certificat. Un adversaire
pouvait donc créer un certificat arbitraire avec un numéro de série et une
chaine émise identique à un certificat fiable que LibreOffice présentait comme
appartenant à l’auteur fiable, éventuellement conduisant l’utilisateur à
exécuter du code arbitraire contenant dans macros considérées improprement
fiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26306">CVE-2022-26306</a>

<p>LibreOffice gère le stockage de mots de passe pour des connexions web dans la
base de données de configuration d’utilisateur. Les mots de passe sont chiffrés
avec une seule clé principale fournie par l’utilisateur. Un défaut existait dans
LibreOffice quand le vecteur d’initialisation requis pour le chiffrement était
toujours le même, ce qui affaiblissait la sécurité du chiffrement et le rendait
vulnérable si un attaquant avait accès aux données de configuration
d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26307">CVE-2022-26307</a>

<p>Ajout de vecteurs d’initialisation pour le stockage de mots de passe.
LibreOffice gère le stockage de mots de passe pour des connexions web dans la
base de données de configuration d’utilisateur. Les mots de passe sont chiffrés
avec une seule clé principale fournie par l’utilisateur. Un défaut dans
LibreOffice existait quand la clé principale était faiblement encodée
aboutissant à un affaiblissement de l’entropie de 128 à 43 bits, rendant les
mots de passe stockés vulnérables à un attaque par brute force si un attaquant
avait accès à la configuration d’utilisateurs stockée.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:6.1.5-3+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libreoffice.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libreoffice,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3368.data"
# $Id: $
