#use wml::debian::translation-check translation="dcaa8d7aaa7db5ce75c379ca0a8fe807950a7535" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que la vérification de domaine dans libmail-dkim-perl,
un module de Perl pour identifier de manière cryptographique l’expéditeur d’un
courriel, comparait les balises i et d en tenant compte de la casse quand t=s
était réglé sur la clé DKIM, ce qui causait des échecs curieux de messages
légitimes.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.54-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libmail-dkim-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libmail-dkim-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libmail-dkim-perl">\
https://security-tracker.debian.org/tracker/libmail-dkim-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3509.data"
# $Id: $
