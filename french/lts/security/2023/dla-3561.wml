#use wml::debian::translation-check translation="71d74590d7de8fed979dcd1e892271b11f33703e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque potentielle par déni de service
par expression rationnelle (ReDoS) dans node-cookiejar, une bibliothèque Node.js
pour analyser et manipuler des cookies HTTP. Une attaque était possible en
passant une grande valeur à la fonction <code>Cookie.parse</code>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25901">CVE-2022-25901</a>

<p>Les versions de paquets cookiejar avant la version 2.1.4 étaient vulnérables
à un déni de service par expression rationnelle de service (ReDoS) à l’aide
de la fonction Cookie.parse qui utilise une expression rationnelle non sûre.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.0.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-cookiejar.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3561.data"
# $Id: $
