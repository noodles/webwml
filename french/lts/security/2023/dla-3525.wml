#use wml::debian::translation-check translation="2f15d750dbe24bc2dbd76259312d66048894f79e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40982">CVE-2022-40982</a>

<p>Daniel Moghimi a découvert que GDS (Gather Data Sampling), une vulnérabilité
matérielle des CPU d’Intel, permettait un accès spéculatif sans privilèges aux
données qui étaient auparavant stockées dans des registres de vecteur.</p>

<p>Cette mitigation nécessite un microcode de CPU mis à jour fourni dans le
paquet intel-microcode.</p>

<p>Pour plus de détails, veuillez consulter
<a href="https://downfall.page/">https://downfall.page/</a> et
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20569">CVE-2023-20569</a>

<p>Daniel Trujillo, Johannes Wikner et Kaveh Razavi ont découvert la faille
INCEPTION, connue également sous le nom de <q>Speculative Return Stack Overflow</q>
(SRSO), une attaque par exécution transitoire qui divulgue des données
arbitraires sur tous les processeurs Zen d'AMD. Un attaquant peut apprendre de
façon erronée au « Branch Target Buffer » (BTB) du processeur à prédire des
instructions CALL non liées à l'architecture dans l'espace du noyau et utiliser
cela pour contrôler la cible spéculative d'une instruction subséquente RET du
noyau, menant éventuellement à la divulgation d'informations à l'aide d'une
attaque spéculative par canal auxiliaire.</p>

<p>Pour plus de détails, veuillez consulter
<a href="https://comsec.ethz.ch/research/microarch/inception/">https://comsec.ethz.ch/research/microarch/inception/</a>
et <a href="https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005">https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005</a>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.179-5~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3525.data"
# $Id: $
