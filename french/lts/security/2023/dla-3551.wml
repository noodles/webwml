#use wml::debian::translation-check translation="5aa48bc2474768dd870951cb0b004b0cd0988388" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans otrs2, le système au code
source ouvert de requêtes par tickets, qui pouvaient conduire à une usurpation
d’identité, un déni de service, une divulgation d'informations ou une
exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11358">CVE-2019-11358</a>

<p>Une vulnérabilité de pollution de prototype a été découverte dans la copie
incorporée de jQuery 3.2.1 d’OTRS. Cela pouvait permettre d’envoyer des messages
ébauchés comme faux agent.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-05.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>

<p>Matthias Terlinde a découvert que quand un attaquant envoyait un courriel
malveillant à un système OTRS et un utilisateur agent connecté le citait plus
tard, le courriel pouvait faire que le navigateur chargeait des ressources
externes d’image.</p>

<p>Un nouveau réglage de configuration
<code>Ticket::Frontend::BlockLoadingRemoteContent</code> a été ajouté comme
partie du correctif. Il contrôle si un contenu externe doit être chargé et il
désactive ce chargement par défaut.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12497">CVE-2019-12497</a>

<p>Jens Meister a découvert que dans le frontal du client ou externe, les
informations personnelles d’agents, telles que le nom et l’adresse de courriel
dans des notes externes, pouvaient être divulguées.</p>

<p>Un nouveau réglage de configuration
<code>Ticket::Frontend::CustomerTicketZoom###DisplayNoteFrom</code> a été
ajouté comme partie du correctif. Il contrôle si une information d’agent doit
être affichée dans le champ sender de note externe ou être remplacée par un
nom générique. Une autre option appelée
<code>Ticket::Frontend::CustomerTicketZoom###DefaultAgentName</code> peut être
utilisée pour définir un nom générique d’agent utilisé dans le dernier cas.
Par défaut, l’ancien comportement est conservé, dans lequel l’information
d’agent est divulguée dans champ From de note externe, pour des raisons de
rétrocompatibilité.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12746">CVE-2019-12746</a>

<p>Un utilisateur connecté dans OTRS comme agent pouvait à son insu divulguer
l’ID de session en partageant le lien dans un article de ticket imbriqué avec
des tierces parties. Cet identifiant pouvait être éventuellement détourné pour
usurper l’identité de l’utilisateur agent.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13458">CVE-2019-13458</a>

<p>Un attaquant connecté dans OTRS comme utilisateur agent avec les permissions
appropriées pouvait exploiter des balises OTRS dans des modèles pour divulguer
les hachages de mot de passe d’utilisateur.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-12.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16375">CVE-2019-16375</a>

<p>Un attaquant connecté dans OTRS comme utilisateur agent ou client avec les
permissions appropriées pouvait créer une chaine soigneusement contrefaite
contenant du code JavaScript malveillant comme corps d’article. Ce code
malveillant était exécuté quand un agent composait une réponse à l’article
original.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-13.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18179">CVE-2019-18179</a>

<p>Un attaquant connecté dans OTRS comme agent pouvait lister les tickets
assignés à d’autres agents, qui sont dans la file d’attente où l’attaquant
n’avait aucune permission.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18180">CVE-2019-18180</a>

<p>OTRS pouvait être entrainé dans une boucle infinie en fournissant des noms de
fichier avec des extensions excessivement longues. Cela s’appliquait au
PostMaster (envoi de courriel) et aussi au téléversement (pièces jointes de
 courriel, par exemple).</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2019-15.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1765">CVE-2020-1765</a>

<p>Sebastian Renker et Jonas Becker ont découvert un contrôle incorrect de
paramètres. Cela permettait l’usurpation de champs From dans plusieurs écrans,
à savoir AgentTicketCompose, AgentTicketForward, AgentTicketBounce et
AgentTicketEmailOutbound.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1766">CVE-2020-1766</a>

<p>Anton Astaf'ev a découvert qu’à cause d’un traitement d’images téléversées,
il était – dans des conditions rares et peu probables – possible de forcer le
navigateur d’agents d’exécuter du code JavaScript malveillant à partir d'un
fichier spécialement contrefait SVG rendu comme fichier jpg interne.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-02.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1767">CVE-2020-1767</a>

<p>Agent A pouvait sauvegarder une ébauche (c’est-à-dire une réponse à un client),
puis Agent B pouvait ouvrir l’ébauche, modifier le texte complètement et
l’envoyer au nom de l’Agent A. Pour le client, il n’était pas possible de voir
que le message était envoyé par un autre agent.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-03.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1769">CVE-2020-1769</a>

<p>Martin Møller a découvert que dans les écrans de connexion (dans les
interfaces agent et client), les champs Username et Password utilisaient
l’autocomplétion, ce qui pouvait être considéré comme un problème de sécurité.</p>

<p>Un réglage de configuration <code>DisableLoginAutocomplete</code> a été
ajouté comme partie du correctif. Il contrôle s’il faut désactiver
l’autocomplétion dans le formulaire de connexion en réglant l’attribut
<code>autocomplete="off"</code> dans les champs d’entrée de connexion. Il est à
remarquer que certains navigateurs l’ignorent par défaut (en général cela peut
être modifié dans la configuration du navigateur).</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-06.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1770">CVE-2020-1770</a>

<p>Matthias Terlinde a découvert que les fichiers de prise en charge de l’offre
groupée générée pouvaient contenir des informations sensibles, telles que les
identifiants.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-07.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1771">CVE-2020-1771</a>

<p>Christoph Wuetschne a découvert qu’un attaquant pouvait contrefaire un
article avec un lien avec le répertoire d’adresses du client avec du contenu
malveillant (JavaScript). Quand un agent ouvrait le lien, le code JavaScript
était exécuté à cause d’un encodage de paramètre manquant.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>

<p>Fabian Henneke a découvert qu’il était possible de contrefaire des requêtes
de mot de passe perdu avec des jokers dans la valeur Token. Cela permettait à un
attaquant de retrouver des Tokens valables générés par des utilisateurs ayant
déjà demandé un nouveau mot de passe.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1773">CVE-2020-1773</a>

<p>Fabian Henneke a découvert qu’un attaquant pouvant générer des ID de session
ou des jetons de réinitialisation de mot de passe, soit en pouvant
s’authentifier, soit en exploitant le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>,
pouvait prédire des ID de session d’autres utilisateurs, des jetons de
réinitialisation de mot de passe et automatiquement générer des mots de passe.</p>

<p>Le correctif ajoute <code>libmath-random-secure-perl</code> au champ Depends:
de otrs2.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1774">CVE-2020-1774</a>

<p>Quand un utilisateur téléchargeait des certificats ou clés PGP ou S/MIME,
le fichier exporté avait le même nom pour les clés privées ou publiques. Il était alors
possible de les mélanger et d’envoyer la clé privée à des parties tierces au
lieu de la clé publique.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-11.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1776">CVE-2020-1776</a>

<p>Quand un utilisateur agent était renommé ou déclaré non valable, la session
appartenant à l’utilisateur était conservée active. La session ne pouvait pas
être utilisée pour accéder aux données de ticket dans le cas où l’agent était
non autorisé.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-13.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11022">CVE-2020-11022</a>

<p>Masato Kinugawa a découvert une vulnérabilité potentielle de script intersite
(XSS) dans htmlPrefilter de jQuery 3.2.1 incorporé dans OTRS et dans les méthodes
relatives.</p>

<p>Le correctif requiert de corriger les copies incorporées de fullcalendar (3.4.0),
fullcalendar-scheduler (1.6.2) et spectrum (1.8.0).</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11023">CVE-2020-11023</a>

<p>Masato Kinugawa a découvert une vulnérabilité potentielle de script intersite
(XSS) dans la copie incorporée de jQuery 3.2.1 d’OTRS lors d’ajout d’éléments
HTML contenant des options.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2020-14.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21252">CVE-2021-21252</a>

<p>Erik Krogh Kristensen et Alvaro Muñoz de l’équipe GitHub Security Lab ont
découvert une vulnérabilité de déni de service par expression rationnelle
(ReDoS) dans la copie incorporée de jQuery-validate 1.16.0 d’OTRS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21439">CVE-2021-21439</a>

<p>Une attaque par déni de service (DoS) pouvait être réalisée quand un courriel
contenait un URL conçu spécialement dans le corps. Elle pouvait conduire à une
utilisation excessive de CPU et une mauvaise qualité de service ou dans des cas extrêmes provoquer l’arrêt du système.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2021-09 ou ZSA-2021-03.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21440">CVE-2021-21440</a>

<p>Julian Droste et Mathias Terlinde ont découvert que le Generated
Support Bundles contenait des clés privées S/MIME et PGP quand le répertoire
parent n’était pas caché. Par conséquent, les secrets et les PIN pour les clés
n’étaient pas masqués proprement.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2021-10 ou ZSA-2021-08.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21441">CVE-2021-21441</a>

<p>Vulnérabilité de script intersite (XSS) dans les écrans d’aperçu de
ticket. Il était possible de collecter diverses informations en ayant un
courriel affiché dans l’écran d’aperçu. Une attaque pouvait être réalisée en
envoyant un courriel contrefait pour l'occasion au système, ne requérant aucune
interaction de l’utilisateur.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2021-11 ou ZSA-2021-06.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21443">CVE-2021-21443</a>

<p>Les agents pouvaient lister des courriels de client sans les permissions
requises dans l’écran d’action en masse.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2021-13 ou ZSA-2021-09.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36091">CVE-2021-36091</a>

<p>Les agents pouvaient lire les engagements dans les agendas, sans les
permissions requises.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2021-14 ou ZSA-2021-10.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36100">CVE-2021-36100</a>

<p>Rayhan Ahmed et Maxime Brigaudeau ont découvert qu’une chaîne contrefaite
pour l'occasion dans la configuration du système permettait l’exécution de
commandes système arbitraires.</p>

<p>Le correctif 1/ supprime les commandes système configurables des agents
génériques ; 2/ supprime les réglages <code>MIME-Viewer###…</code> (la commande
système dans l’option SysConfig <code>MIME-Viewer</code> est désormais
configurable à l’aide de Kernel/Config.pm) ; 3/ supprime la prise en charge de
widgets de tableau de bord pour l’exécution de commandes système ; 4/ désactive
la prise en charge de l’exécution de commandes système des configurations
préfiltrées de Sendmail et PostMaster.</p>

<p>Cette vulnérabilité est aussi connue comme OSA-2022-03 ou ZSA-2022-02.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41182">CVE-2021-41182</a>

<p>Esben Sparre Andreasen a découvert une vulnérabilité de script intersite
(XSS) dans l’option <code>altField</code> du widget Datepicker dans la
copie incorporée de jQuery-UI 1.12.1 d’OTRS.</p>

<p>Cette vulnérabilité est aussi connue comme ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41183">CVE-2021-41183</a>

<p>Esben Sparre Andreasen a découvert une vulnérabilité de script intersite
(XSS) dans l’option <code>*Text</code> du widget Datepicker dans la
copie incorporée de jQuery-UI 1.12.1 d’OTRS.</p>

<p>Cette vulnérabilité est aussi connue comme ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41184">CVE-2021-41184</a>

<p>Esben Sparre Andreasen a découvert une vulnérabilité de script intersite
(XSS) dans l’option <code>of</code> de l’outil <code>.position()</code> dans la
copie incorporée de jQuery-UI 1.12.1 d’OTRS.</p>

<p>Cette vulnérabilité est aussi connue comme ZSA-2022-01.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4427">CVE-2022-4427</a>

<p>Tim Püttmanns a découvert une vulnérabilité d’injection SQL dans
<code>Kernel::System::Ticket::TicketSearch</code>, qui pouvait être exploitée
en utilisant l’opération de service web <q>TicketSearch</q>.</p>

<p>Cette vulnérabilité est aussi connue comme ZSA-2022-07.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38060">CVE-2023-38060</a>

<p>Tim Püttmanns a découvert une vulnérabilité de validation d’entrée impropre
dans le paramètre ContentType pour les pièces jointes dans les opérations
TicketCreate ou TicketUpdate.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 6.0.16-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de otrs2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/otrs2">\
https://security-tracker.debian.org/tracker/otrs2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3551.data"
# $Id: $
