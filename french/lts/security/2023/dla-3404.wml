#use wml::debian::translation-check translation="4ce24529ecc45607ffa9ddf3040f15c90b64f363" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation des privilèges, un déni de service ou à
une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2196">CVE-2022-2196</a>

<p>Une régression a été découverte dans l’implémentation de KVM pour les CPU
d’Intel, affectant la virtualisation imbriquée pour Spectre v2. Quand KVM était
utilisé comme hyperviseur L0, un invité L2 pouvait exploiter cela pour divulguer
des informations sensibles de son hyperviseur L.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3424">CVE-2022-3424</a>

<p>Zheng Wang et Zhuorao Yang ont signalé un défaut dans le pilote GRU SGI
qui pouvait conduire à une utilisation de mémoire après libération. Sur les
systèmes où ce pilote était utilisé, un utilisateur local pouvait exploiter cela
pour un déni de service (plantage ou corruption de mémoire) ou, éventuellement,
pour une élévation des privilèges.</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau dans
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3707">CVE-2022-3707</a>

<p>Zheng Wang a signalé un défaut dans la prise en charge de la virtualisation
du pilote graphique i915(GVT-g), qui pouvait conduire à une double libération
de zone de mémoire. Sur les systèmes où cette fonctionnalité était utilisée,
un invité pouvait exploiter cela pour un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4129">CVE-2022-4129</a>

<p>Haowei Yan a signalé une situation de compétition dans l’implémentation du
protocole L2TP, qui pouvait conduire à un déréférencement de pointeur NULL. Un
utilisateur local pouvait exploiter cette situation pour un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4379">CVE-2022-4379</a>

<p>Xingyuan Mo a signalé un défaut dans l’implémentation NFSv4.2 <q>inter
serveur</q> vers <q>serveur copy</q>,  qui pouvait conduire à une utilisation
de mémoire après libération.</p>

<p>Cette fonctionnalité n’est pas activée dans les configurations officielles du
noyau dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0045">CVE-2023-0045</a>

<p>Rodrigo Branco et Rafael Correa De Ysasi ont signalé qu’une tâche en espace
utilisateur demandait au noyau d’activer la mitigation de Spectre v2, Cette
mitigation n’était pas activée avant que la tâche ne soit reprogrammée. Cela
pouvait être exploitable par un attaquant local ou distant pour divulguer des
informations sensibles à partir d’une telle application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0458">CVE-2023-0458</a>

<p>Jordy Zimmer et Alexandra Sandulescu ont trouvé que getrlimit() et les
appels système relatifs étaient vulnérables à des attaques d’exécution
spéculative telles que Spectre v1. Un utilisateur local pouvait exploiter cela
pour une divulgation d’informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0459">CVE-2023-0459</a>

<p>Jordy Zimmer et Alexandra Sandulescu ont trouvé une régression dans la
mitigation de Spectre v1 dans les fonctions <q>user-copy</q> pour l’architecture
amd64 (PC 64 bits). Quand les CPU ne mettaient pas en œuvre SMAP ou que celui-ci
était désactivé, un utilisateur local pouvait exploiter cela pour une
divulgation d’informations sensibles du noyau. D’autres architectures pouvaient
aussi être affectées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0461">CVE-2023-0461</a>

<p><q>slipper</q> a signalé un défaut dans la prise en charge du noyau des ULP
(Upper protocole Layer) au-dessus de TCP, qui pouvait conduire à une double
libération de zone de mémoire lors de l’utilisation des sockets TLS du noyau.
Un utilisateur local pouvait exploiter cela pour un déni de service (plantage
ou corruption de mémoire) ou, éventuellement, pour une élévation des privilèges.</p>

<p>TLS du noyau n’est pas activé dans les configurations officielles du noyau
dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1073">CVE-2023-1073</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans le sous-système
HID (Human Interface Device). Un attaquant capable d’insérer et de retirer des
périphériques USB pouvait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou, éventuellement, pour exécuter du code
arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1074">CVE-2023-1074</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans
l’implémentation du protocole SCTP, qui pouvait amener à une fuite de mémoire. Un
utilisateur local pouvait exploiter cela pour provoquer un déni de service
(épuisement de ressources).</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1076">CVE-2023-1076</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans
l’implémentation du pilote réseau TUN/TAP, qui aboutissait à ce que tous les
sockets TUN/TAP étaient marqués comme appartenant à l’utilisateur d’ID 0 (root).
Cela permettait aux utilisateurs locaux de s’affranchir des règles locales de
pare-feu basées sur l’ID de l’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1077">CVE-2023-1077</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans l’ordonnanceur
de tâches. Un utilisateur local pouvait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1078">CVE-2023-1078</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans
l’implémentation du protocole RDS. Un utilisateur local pouvait exploiter cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1079">CVE-2023-1079</a>

<p>Pietro Borrello a signalé une situation de compétition dans le pilote HID
hid-asus, qui pouvait conduire à une utilisation de mémoire après libération.
Un attaquant capable d’insérer ou de retirer des périphériques USB pouvait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1118">CVE-2023-1118</a>

<p>Duoming Zhou a signalé une situation de compétition dans le pilote de
contrôle distant ene_ir, qui pouvait conduire à une utilisation de mémoire après
libération si le pilote était détaché. L’impact de sécurité de ce problème
n’est pas très défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1281">CVE-2023-1281</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2023-1829">CVE-2023-1829</a>

<p><q>valis</q> a signalé deux défauts dans le classificateur de trafic réseau
cls_tcindex network, qui pouvaient conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour une élévation des
privilèges. Cette mise à jour retire complètement cls_tcindex.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1513">CVE-2023-1513</a>

<p>Xingyuan Mo a signalé une fuite d'informations dans l’implémentation de KVM
pour l’architecture i386 (PC 32 bits). Un utilisateur local pouvait exploiter
cela pour divulguer des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1611">CVE-2023-1611</a>

<p><q>butt3rflyh4ck</q> a signalé une situation de compétition dans le pilote
de système de fichiers btrfs, qui pouvait conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait utiliser cela pour provoquer un
déni de service (plantage ou corruption de mémoire) ou, possiblement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1670">CVE-2023-1670</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote réseau
xirc2ps_cs network, qui pouvait conduire à une utilisation de mémoire après
libération. Un attaquant capable d’insérer ou de retirer des périphériques PMCIA
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1855">CVE-2023-1855</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote de
supervision matérielle xgene-hwmon, qui pouvait conduire à une utilisation de
mémoire après libération. L’impact de sécurité de ce problème n’est pas très
défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1859">CVE-2023-1859</a>

<p>Zheng Wang a signalé une situation de compétition dans le transport 9pnet_xen
pour le système de fichiers 9P dans Xen, qui pouvait conduire à une utilisation
de mémoire après libération. Dans les systèmes où cette fonctionnalité était
utilisée, un pilote de dorsal dans un autre domaine pouvait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour exécuter du code arbitraire dans le domaine vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1872">CVE-2023-1872</a>

<p>Bing-Jhong Billy Jheng a signalé une situation de compétition dans le
sous-système io_uring, qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait utiliser cela pour provoquer un
déni de service (plantage ou corruption de mémoire) ou, possiblement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1989">CVE-2023-1989</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote btsdio
d’adaptateur Bluetooth, qui pouvait conduire à une utilisation de mémoire après
libération. Un attaquant capable d’insérer ou de retirer des périphériques SDIO
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1990">CVE-2023-1990</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote st-nci
d’adaptateur NFC, qui pouvait conduire à une utilisation de mémoire après
libération. L’impact de sécurité de ce problème n’est pas très
défini.</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau
dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1998">CVE-2023-1998</a>

<p>José Oliveira et Rodrigo Branco ont signalé une régression dans la
mitigation de Spectre v2 pour l’espace utilisateur pour les CPU x86 gérant
IBRS mais pas eIBRS. Cela pouvait être exploitable par un attaquant local ou
distant pour divulguer des informations sensibles d’une application en espace
utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2162">CVE-2023-2162</a>

<p>Mike Christie a signalé une situation de compétition dans le transport iSCSI
TCP, qui pouvait conduire à une utilisation de mémoire après libération. Dans
les systèmes où cette fonctionnalité était utilisée, un utilisateur local
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2194">CVE-2023-2194</a>

<p>Wei Chen a signalé un dépassement potentiel de tampon de tas dans le pilote
i2c-xgene-slimpro d’adaptateur IÂ²C. Un utilisateur local avec permission
d’accès à de tels périphériques, pouvait utiliser cela pour provoquer un déni
de service (plantage ou corruption de mémoire) et, probablement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22998">CVE-2023-22998</a>

<p>Miaoqian Lin a signalé une vérification incorrecte d’erreur dans le pilote
virtio-gpu GPU. Un utilisateur local avec permission d’accès à de tels
périphériques, pouvait utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23004">CVE-2023-23004</a>

<p>Miaoqian Lin a signalé une vérification d'erreur incorrecte dans le pilote
mali-dp GPU. Un utilisateur local avec accès à ce périphérique pouvait
utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23559">CVE-2023-23559</a>

<p>Szymon Heidrich a signalé une vérification incorrecte de limites dans le
pilote Wi-Fi rndis_wlan, qui pouvait conduire à un dépassement de tampon de tas
ou une lecture hors limites. Un attaquant capable d’insérer ou de retirer des
périphériques USB pouvait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou une fuite d'informations, ou,
éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25012">CVE-2023-25012</a>

<p>Pietro Borrello a signalé une situation de compétition dans le pilote HID
hid-bigbenff, qui pouvait conduire à une utilisation de mémoire après libération.
Un attaquant capable d’insérer ou de retirer des périphériques USB pouvait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26545">CVE-2023-26545</a>

<p>Lianhui Tang a signalé un défaut dans l’implémentation du protocole MPLS, qui
pouvait conduire à une double libération de zone de mémoire. Un utilisateur
local pouvait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou, possiblement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28328">CVE-2023-28328</a>

<p>Wei Chen a signalé un défaut dans le pilote az6927 DVB, qui pouvait conduire
à un déréférencement de pointeur NULL. Un utilisateur local pouvant accéder
à un périphérique adaptateur IÂ²C que ce pilote créait pouvait utiliser cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28466">CVE-2023-28466</a>

<p>Hangyu Hua a signalé une situation de compétition dans l’implémentation de
socket TLS du noyau, qui pouvait conduire à une utilisation de mémoire après
libération ou à un déréférencement de pointeur NULL. Un utilisateur local
pouvait exploiter cela pour un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour une élévation des privilèges.</p>

<p>Cette fonctionnalité n’est pas activée dans les configurations officielles
du noyau dans Debian.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30456">CVE-2023-30456</a>

<p>Reima ISHII a signalé un défaut dans l’implémentation de KVM pour les CPU
d’Intel affectant la virtualisation imbriquée. Quand KVM était utilisé comme
hyperviseur L0 et que EPT et/ou le mode invité non restreint était désactivé,
cela n’empêchait pas un invité L2 d’être configuré avec le mode protection/paging
architecturellement non valable. Un invité malfaisant pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.178-3~deb10u1. De plus, cette mise à jour corrige les bogues
Debian <a href="https://bugs.debian.org/989705">n° 989705</a>,
<a href="https://bugs.debian.org/993612">n° 993612</a>,
<a href="https://bugs.debian.org/1022126">n° 1022126</a> et
<a href="https://bugs.debian.org/1031753">n° 1031753</a> et inclut d’autres
corrections de bogues à partir des mises à jour de stable,
version 5.10.163-5.10.178 incluse.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3404.data"
# $Id: $
