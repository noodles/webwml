#use wml::debian::translation-check translation="2074507438f8d3a39ad2f8b50d8f85001981f664" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité potentielle de script intersite (XSS)
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-36180">CVE-2022-36180</a>) et une vulnérabilité de traitement de session
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-36179">CVE-2022-36179</a>)
ont été découvertes dans fusiondirectory, un programme d’administration LDAP
basée sur le web.</p>

<p>De plus, fusiondirectory a été mis à niveau pour corriger la modification
d’API dans php-cas à cause du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>.
Consulter la DLA 3485-1 pour plus de détails.</p>

<p>Dû à cela, si l’authentification CAS est utilisée, fusiondirectory ne
fonctionnera plus avant que ces deux étapes aient été réalisées :</p>

<p>— installation du nouveau paquet fusiondirectory-schema pour Buster</p>

<p>— mise à jour du schéma central de fusiondirectory dans LDAP en exécutant
fusiondirectory-insert-schema -m</p>

<p>— basculer vers la nouvelle API de php-cas en exécutant
fusiondirectory-setup --set-config-CasLibraryBool=TRUE</p>

<p>— régler le ClientServiceName CAS à l’URL de base dans l’installation de
fusiondirectory, par exemple :
fusiondirectory-setup --set-config-CasClientServiceName="https://fusiondirectory.example.org/"</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.2.3-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets fusiondirectory.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de fusiondirectory,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/fusiondirectory">\
https://security-tracker.debian.org/tracker/fusiondirectory</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3487.data"
# $Id: $
