#use wml::debian::translation-check translation="cd918c29d18c054833d56c0ca952be540bbba5f6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui pouvaient
conduire à une élévation des privilèges, un déni de service ou une fuite
d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0386">CVE-2023-0386</a>

<p>Il a été découvert que sous certaines conditions l’implémentation du
système de fichiers overlayfs ne gérait pas correctement les opérations de copie.
Un utilisateur local autorisé pour des montages overlay dans l’espace de
nommage utilisateur pouvait exploiter ce défaut pour une élévation locale des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31436">CVE-2023-31436</a>

<p>Gwangun Jung a signalé un défaut provoquant des erreurs de lecture/écriture
hors limites dans le sous-système de contrôle de trafic pour l’ordonnanceur QFQ
(Quick Fair Queueing), qui pouvaient aboutir à une fuite d'informations, un déni
de service ou une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32233">CVE-2023-32233</a>

<p>Patryk Sondej et Piotr Krysiuk ont découvert un défaut d’utilisation de
mémoire après libération dans l’implémentation Netfilter de nf_tables lors du
traitement de requêtes par lot, qui pouvait aboutir à une élévation locale des
privilèges pour un utilisateur ayant la capacité CAP_NET_ADMIN dans n’importe
quel espace de nommage utilisateur ou réseau.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.179-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3446.data"
# $Id: $
