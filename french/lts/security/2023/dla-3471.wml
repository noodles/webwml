#use wml::debian::translation-check translation="4e0ae2b6c2605bfab5e821e617fe1bc8f1a0c809" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans c-ares, une bibliothèque de
résolution de noms asynchrone.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31130">CVE-2023-31130</a>

<p>ares_inet_net_pton() a été découverte comme vulnérable à un dépassement de
capacité de tampon par le bas pour des adresses IPv6, en particulier
<q>0::00:00:00/2</q> pouvait poser un problème. c-ares utilise uniquement cette
fonction en interne dans des buts de configuration, cependant un usage externe
pour d’autres buts pouvait causer des problèmes plus graves.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32067">CVE-2023-32067</a>

<p>Le résolveur de cible pouvait interpréter de manière erronée un paquet UDP
mal formé ayant une taille de 0 comme une fermeture graduelle de connexion, ce
qui pouvait provoquer un déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.14.0-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets c-ares.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de c-ares,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/c-ares">\
https://security-tracker.debian.org/tracker/c-ares</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3471.data"
# $Id: $
