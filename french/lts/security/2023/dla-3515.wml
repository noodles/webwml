#use wml::debian::translation-check translation="7c1d0dd7f331c5e7f8bda10dd413c04b6d91f5dd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une utilisation de longueur incorrecte d’étiquette d’authentification a été
découverte dans cjose, une bibliothèque C mettant en œuvre la norme JOSE
(Javascript Object Signing and Encryption), qui pouvait conduire à une
compromission d’intégrité.</p>

<p>La routine de déchiffrement AES GCM utilisait incorrectement la longueur
d’étiquette à partir de l’étiquette réelle d’authentification telle que fournie
par un objet JWE (JSON Web Encryption), alors que la
<a href="https://www.rfc-editor.org/rfc/rfc7518#section-4.7">spécification</a>
indique qu’une longueur fixe de 16 octets doit être utilisée. Cela pouvait
permettre à un attaquant de fournir une étiquette tronquée et de modifier le
JWE en conséquence.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.6.1+dfsg1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cjose.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cjose,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cjose">\
https://security-tracker.debian.org/tracker/cjose</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3515.data"
# $Id: $
