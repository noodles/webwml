#use wml::debian::translation-check translation="af62f3aa40f299f14972465999d50b3dfa98f4f3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL,une boîte à
outils SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0464">CVE-2023-0464</a>

<p>David Benjamin a signalé un défaut lié à la vérification de chaînes de
certificats X.509 qui incluent des restrictions de politique. Cela peut
avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0465">CVE-2023-0465</a>

<p>David Benjamin a signalé que des politiques de certificats non valables
dans les certificats d'entité finale sont ignorées en silence. Une autorité
de certification malveillante peut tirer avantage de ce défaut pour
déclarer délibérément des politiques de certificat non valables afin de
contourner complètement la vérification de politique sur le certificat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0466">CVE-2023-0466</a>

<p>David Benjamin a découvert que l'implémentation de la fonction
X509_VERIFY_PARAM_add0_policy() n'activait pas la vérification, permettant
à des certificats avec des politiques non valables ou incorrectes de
passer la vérification de certificats (contrairement à ce que dit sa
documentation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2650">CVE-2023-2650</a>

<p>Le traitement d'identificateurs d'objet ASN.1 mal formés ou des données
qui les contiennent peut avoir pour conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.1.1n-0+deb11u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5417.data"
# $Id: $
