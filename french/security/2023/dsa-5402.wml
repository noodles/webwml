#use wml::debian::translation-check translation="1eea0981a72fd2056f916b32ce526e926c999d7d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0386">CVE-2023-0386</a>

<p>Dans certaines conditions, l'implémentation du système de fichiers
OverlayFS ne gérait pas correctement les opérations <q>copy up</q>. Un
utilisateur local autorisé à effectuer des montages <q>overlay</q> dans les
espaces de noms de l'utilisateur pouvait tirer avantage de ce défaut pour
une élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31436">CVE-2023-31436</a>

<p>Gwangun Jung a signalé un défaut provoquant des erreurs de 
lecture/écriture de tas hors limites dans le sous-système de contrôle de
trafic de l'ordonnanceur Quick Fair Queueing (QFQ), ce qui pouvait avoir
pour conséquences une fuite d'informations, un déni de service ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32233">CVE-2023-32233</a>

<p>Patryk Sondej et Piotr Krysiuk ont découvert un défaut d'utilisation de
mémoire après libération dans l'implémentation de Netfilter nf_tables lors
du traitement de requêtes par lot, ce qui pouvait avoir pour conséquences
une élévation locale de privilèges pour un utilisateur doté de la capacité
CAP_NET_ADMIN dans n'importe quel espace de noms utilisateur ou réseau.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.179-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5402.data"
# $Id: $
