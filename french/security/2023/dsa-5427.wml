#use wml::debian::translation-check translation="12b42f1bacd115bf4fe0f13e4edb579ccaf7aa86" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les deux vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28204">CVE-2023-28204</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web peut
divulguer des informations sensibles. Apple a eu connaissance d'un rapport
indiquant que ce problème peut avoir été activement exploité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32373">CVE-2023-32373</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pouvait conduire à l'exécution de code arbitraire. Apple a eu
connaissance d'un rapport indiquant que ce problème peut avoir été
activement exploité.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 2.40.2-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 2.40.2-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5427.data"
# $Id: $
