#use wml::debian::translation-check translation="925ae76facb2d1bc28343b8f7956afe7f13c0a12" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Clôture de DebConf22 à Prizren et annonce des dates de DebConf23</define-tag>

<define-tag release_date>2022-07-24</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Aujourd'hui, dimanche 24 juillet 2022, la conférence annuelle des développeurs
et contributeurs Debian s'est achevée.
Accueillant 260 participants venus de 38 pays différents, et plus
de 91 événements combinant communications, séances de discussion ou sessions
spécialisées (BoF), ateliers et activités,
<a href="https://debconf22.debconf.org">DebConf22</a> a été un énorme succès.
</p>

<p>
La conférence a été précédée par le DebCamp annuel, du 10 au 16 juillet qui a
mis l'accent sur des travaux individuels et des rencontres d'équipes pour une
collaboration directe au développement de Debian. En particulier, cette année,
il y a eu des rencontre pour faire avancer le développement de Mobian/Debian sur
téléphone portable, des constructions reproductibles et Python dans Debian, et
une formation intensive pour les nouveaux venus pour les introduire à Debian et
pour qu'ils acquièrent une expérience pratique de son utilisation et de
contribution à la communauté.
</p>

<p>
La conférence annuelle des développeurs Debian proprement dite a débuté samedi
17 juillet 2022. Conjointement à des séances plénières telles que les
traditionnelles « brèves du chef du projet », une séance permanente de signature
de clés, les présentations éclair et l'annonce de la conférence de l'année
prochaine, (<a href="https://wiki.debian.org/DebConf/23">DebConf23</a> qui se
tiendra à Kochi, en Inde), diverses séances se sont tenues liées aux équipes de
langages de programmation comme Python, Perl et Ruby, ainsi que des nouvelles
informations de plusieurs projets et d'équipes internes de Debian, des sessions
de discussions (BoFs) de plusieurs équipes techniques (Long Term Support, 
outils Android, Distributions dérivées de Debian, Installateur Debian et équipe
Images, Debian Science...) et des communautés locales (Debian Brésil, Debian
Inde, les Équipes locale de Debian), ainsi que de nombreux autres événements
d'intérêt concernant Debian et le logiciel libre.
</p>

<p>
Le <a href="https://debconf22.debconf.org/schedule/">programme</a>
a été mis à jour chaque jour avec des activités planifiées et improvisées
introduites par les participants pendant toute la durée de la conférence.
Plusieurs activités qui n'avaient pu être organisées les années précédentes du
fait de la pandémie de COVID ont fait leur retour dans le programme de la
conférence : une bourse d'emploi, un soirée poésie et scène ouverte, la fête
traditionnelle du fromage et du vin, les photos de groupe et la journée
d'excursion.
</p>

<p>
Pour tous ceux qui n'ont pu participer, la plupart des communications et des
sessions ont été enregistrées pour une diffusion en direct avec des vidéos
rendues disponibles sur le
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/">site web des réunions Debian</a>.
Presque toutes les sessions ont facilité la participation à distance par des
applications de messagerie IRC ou un éditeur de texte collaboratif en ligne.
</p>

<p>
Le site web de <a href="https://debconf22.debconf.org/">DebConf22</a>
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.
</p>

<p>
L'an prochain, <a href="https://wiki.debian.org/DebConf/23">DebConf23</a> se
tiendra à Kochi en Inde du 10 au 16 septembre 2023. Comme à l'accoutumée, les
organisateurs débuteront les travaux en Inde, avant la DebConf, par un DebCamp
(du 3 au 9 septembre 2023), avec un accent particulier mis sur le travail
individuel ou en équipe pour améliorer la distribution. 
</p>

<p>
DebConf s'est engagée à offrir un environnement sûr et accueillant pour tous
les participants. 
Voir <a href="https://debconf22.debconf.org/about/coc/">la page web à propos du
Code de conduite sur le site de DebConf22</a> pour plus de détails à ce sujet.
</p>

<p>
Debian remercie les nombreux
<a href="https://debconf22.debconf.org/sponsors/">parrains</a>
pour leur engagement dans leur soutien à DebConf22, et en particulier nos
parrains de platine :
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com/">ITP Prizren</a>
et <a href="https://google.com/">Google</a>.
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est devenu
l'un des plus importants et des plus influents projets à code source ouvert.
Des milliers de volontaires du monde entier travaillent ensemble pour créer et
maintenir les logiciels Debian. Traduite en soixante-dix langues et gérant un
grand nombre de types d'ordinateurs, la distribution Debian est conçue pour
être le <q>système d'exploitation universel</q>.
</p>

<h2>À propos de DebConf</h2>
<p>
DebConf est la conférence des développeurs du projet Debian. En plus d'un
programme complet de présentations techniques, sociales ou organisationnelles,
DebConf fournit aux développeurs, aux contributeurs et à toutes personnes
intéressées, une occasion de rencontre et de travail collaboratif interactif.
DebConf a eu lieu depuis 2000 en des endroits du monde aussi variés que
l'Écosse, l'Argentine et la Bosnie-Herzegovine. Plus de renseignements sont
disponibles sur <a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>À propos de Lenovo</h2>
<p>
En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des machines de
bureau et des stations de travail aussi bien que des périphériques de réalité
augmentée et de réalité virtuelle, des solutions pour la domotique ou le bureau
connecté et les centres de données, <a href="https://www.lenovo.com">Lenovo</a>
comprend combien sont essentiels les plateformes et systèmes ouverts pour un
monde connecté.
</p>

<h2>À propos d'Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> est la plus grande compagnie
suisse d'hébergement web qui offre aussi des services de sauvegarde et de
stockage, des solutions pour les organisateurs d'événement et de services de
diffusion en direct et de vidéo à la demande. Elle possède l'intégralité de ses
centres de données et de tous les éléments essentiels pour le fonctionnement des
services et produits qu'elle fournit (à la fois sur le plan matériel et
logiciel).
</p>

<h2>À propos d'ITP Prizren</h2>
<p>
L'<a href="https://itp-prizren.com/">Innovation and Training Park Prizren</a>
ambitionne d'être un facteur du changement et de stimulation dans le domaine des
technologies de l'information et de la communication, l'agroalimentaire et les
industries créatives au moyen de la création et de la gestion d'un environnement
favorable et de services efficaces pour les PME, en exploitant différents types
d'innovations qui peuvent contribuer à améliorer le développement de l'industrie
et de la recherche du Kosovo, apportant des bénéfices à l'économie et à la
société du pays dans son ensemble.
</p>

<h2>À propos de Google</h2>
<p>
<a href="https://google.com/">Google</a> est notre quatrième parrain de platine.
Google est l'une des plus grandes entreprises technologiques du monde qui
fournit une large gamme de services relatifs à Internet et de produits tels
que des technologies de publicité en ligne, des outils de recherche, de
l'informatique dans les nuages, des logiciels et du matériel. Google apporte
son soutien à Debian en parrainant la DebConf depuis plus de dix ans, et
est également un partenaire Debian parrainant des éléments de l'infrastructure
d'intégration continue de <a href="https://salsa.debian.org">Salsa</a> au sein
de la plateforme Google Cloud.
</p>

<h2>Plus d'informations</h2>

<p>
Pour plus d'informations, veuillez consulter la page internet de
DebConf 22 à l'adresse
<a href="https://debconf22.debconf.org/">https://debconf22.debconf.org/</a>
ou écrire à &lt;press@debian.org&gt;.
</p>
